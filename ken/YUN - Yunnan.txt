﻿capital = 325
oob = "YUN_1936"
set_technology = {
  infantry_weapons = 1
}
if = {
  limit = {
    has_dlc = "Waking the Tiger"
  }
  add_ideas = {
    PRC_government_corruption
  }
}
1939.1.1 = {
  add_political_power = 1198
  CHI = {
    add_to_faction = PREV
  }
  add_to_war = {
    targeted_alliance = CHI
    enemy = JAP
    hostility_reason = asked_to_join
  }
  if = {
    limit = {
      NOT = {
        has_dlc = "Waking the Tiger"
      }
    }
    complete_national_focus = army_effort
    complete_national_focus = equipment_effort
    complete_national_focus = motorization_effort
    complete_national_focus = aviation_effort
    complete_national_focus = construction_effort_2
    complete_national_focus = production_effort_2
    complete_national_focus = infrastructure_effort
    complete_national_focus = industrial_effort
    complete_national_focus = construction_effort
    complete_national_focus = production_effort
  }
  oob = "YUN_1939"
  set_technology = {
    grand_battle_plan = 1
    trench_warfare = 1
    electronic_mechanical_engineering = 1
    radio = 1
    radio_detection = 1
    mechanical_computing = 1
    basic_machine_tools = 1
    improved_machine_tools = 1
    advanced_machine_tools = 1
    fuel_silos = 1
    construction1 = 1
    construction2 = 1
    dispersed_industry = 1
    dispersed_industry2 = 1
  }
}
set_popularities = {
  democratic = 0
  fascism = 20
  communism = 0
  neutrality = 80
}
set_politics = {
  ruling_party = neutrality
  last_election = "1936.1.1"
  election_frequency = 48
  elections_allowed = no
}
create_country_leader = {
  name = "龍雲"  # Long Yun
  desc = "POLITICS_LONG_YUN_DESC"
  picture = "portrait_yun_long_yun.dds"
  expire = "1965.1.1"
  ideology = despotism
  traits = {}
}
create_country_leader = {
  name = "龍雲"  # Long Yun
  desc = "POLITICS_LONG_YUN_DESC"
  picture = "portrait_yun_long_yun.dds"
  expire = "1965.1.1"
  ideology = fascism_ideology
  traits = {}
}
create_country_leader = {
  name = "龍雲"  # Long Yun
  desc = "POLITICS_LONG_YUN_DESC"
  picture = "portrait_yun_long_yun.dds"
  expire = "1965.1.1"
  ideology = conservatism
  traits = {}
}
create_country_leader = {
  name = "龍雲"  # Long Yun
  desc = "POLITICS_LONG_YUN_DESC"
  picture = "portrait_yun_long_yun.dds"
  expire = "1965.1.1"
  ideology = marxism
  traits = {}
}
create_country_leader = {
  name = "龍雲"  # Long Yun
  desc = "POLITICS_LONG_YUN_DESC"
  picture = "portrait_yun_long_yun.dds"
  expire = "1965.1.1"
  ideology = n_socialism_ideology
  traits = {}
}
create_field_marshal = {
  name = "龍雲"  # Long Yun
 #desc = "1884年11月19日生於雲南省恩安縣，1962年6月27日於北京病逝，雲南陸軍講武堂第4期畢業，1935年4月3日敘任陸軍二級上將"
  picture = "portrait_yun_long_yun.dds"
  traits = { logistics_wizard }
  skill = 3
  attack_skill = 2
  defense_skill = 2
  planning_skill = 3
  logistics_skill = 4
}
create_corps_commander = {
  name = "盧漢"  # Lu Han
 #desc = "1896年2月6日生於雲南省恩安縣，1974年5月13日於北京病逝，雲南陸軍講武堂，1939年5月13日敘任陸軍中將加上將銜" 
  picture = "Portrait_Yunnan_Lu_Han.dds"
  traits = { infantry_officer }
  skill = 3
  attack_skill = 2
  defense_skill = 3
  planning_skill = 2
  logistics_skill = 3
}
create_corps_commander = {
  name = "孫渡"  # Sun Du
 #desc = "1898年5月5日生於雲南省陸涼州，1967年4月於昆明病逝，雲南陸軍講武堂，1936年4月18日敘任陸軍中將" 
  picture = "Portrait_yun_Sun_Du.dds"
  traits = {}
  skill = 2
  attack_skill = 2
  defense_skill = 2
  planning_skill = 3
  logistics_skill = 2
}
#create_corps_commander = {
#  name = "張沖" # Zhang Chong 少將 1936.4.1
#  picture = ""
#  traits = {}
#  skill = 2
#  attack_skill = 2
#  defense_skill = 2
#  planning_skill = 2
#  logistics_skill = 1
#}
1939.1.1 = {
  set_popularities = {
    democratic = 0
    fascism = 20
    communism = 0
    neutrality = 80
  }
  set_politics = {
    ruling_party = neutrality
    last_election = "1936.1.1"
    election_frequency = 48
    elections_allowed = no
  }
}