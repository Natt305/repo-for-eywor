﻿capital = 760
oob = "NXM_1936"
set_stability = 0.7
set_technology = {
  infantry_weapons = 1
  mass_assault = 1
}
add_ideas = MON_nomadic
1939.1.1 = {
  add_to_war = {
    targeted_alliance = CHI
    enemy = JAP
  }
  add_political_power = 1198
  if = {
    limit = {
      NOT = {
        has_dlc = "Waking the Tiger"
      }
    }
    complete_national_focus = army_effort
    complete_national_focus = equipment_effort
    complete_national_focus = motorization_effort
    complete_national_focus = aviation_effort
    complete_national_focus = construction_effort_2
    complete_national_focus = production_effort_2
    complete_national_focus = infrastructure_effort
    complete_national_focus = industrial_effort
    complete_national_focus = construction_effort
    complete_national_focus = production_effort
  }
  add_ideas = {
    tot_economic_mobilisation
    service_by_requirement
    closed_economy
  }
  oob = "NXM_1939"
  set_technology = {
    air_superiority = 1
    pocket_defence = 1
    defence_in_depth = 1
    fleet_in_being = 1
    battlefleet_concentration = 1
    convoy_sailing = 1
    electronic_mechanical_engineering = 1
    radio = 1
    radio_detection = 1
    mechanical_computing = 1
    computing_machine = 1
    basic_encryption = 1
    basic_decryption = 1
    basic_machine_tools = 1
    improved_machine_tools = 1
    advanced_machine_tools = 1
    synth_oil_experiments = 1
    oil_processing = 1
    improved_oil_processing = 1
    construction1 = 1
    construction2 = 1
    construction3 = 1
    concentrated_industry = 1
    concentrated_industry2 = 1
    concentrated_industry3 = 1
  }
  set_popularities = {
    democratic = 0
    fascism = 0
    communism = 0
    neutrality = 100
  }
  set_politics = {
    ruling_party = neutrality
    last_election = "1936.1.1"
    election_frequency = 48
    elections_allowed = no
  }
}
set_popularities = {
  democratic = 0
  fascism = 0
  communism = 0
  neutrality = 100
}
set_politics = {
  ruling_party = neutrality
  last_election = "1936.1.1"
  election_frequency = 48
  elections_allowed = no
}
create_country_leader = {
  name = "馬鴻賓"  # Ma Hongbin
  desc = "POLITICS_MA_HONGBIN_DESC"
  picture = "gfx/leaders/MON/Portrait_Ma_Hongbin.dds"
  expire = "1952.1.1"
  ideology = despotism
  traits = {}
}
create_country_leader = {
  name = "馬鴻逵"  # Ma Hongkui
  desc = "POLITICS_MA_HONGKUI_DESC"
  picture = "Portrait_Xibei_San_Ma_Ma_Hongkui.dds"
  expire = "1975.7.1"
  ideology = despotism
  traits = {}
}
create_country_leader = {
  name = "高樹勛"  # Gao Shuxun
  desc = ""
  picture = "Portrait_Xibei_San_Ma_Gao_Shuxun.dds"
  expire = "1975.7.1"
  ideology = marxism
  traits = {}
}
create_country_leader = {
  name = "馬鴻逵"  # Ma Hongkui
  desc = "POLITICS_MA_HONGKUI_DESC"
  picture = "Portrait_Xibei_San_Ma_Ma_Hongkui.dds"
  expire = "1970.1.20"
  ideology = marxism
}
create_country_leader = {
  name = "馬鴻逵"  # Ma Hongkui
  desc = "POLITICS_MA_HONGKUI_DESC"
  picture = "Portrait_Xibei_San_Ma_Ma_Hongkui.dds"
  expire = "1970.1.20"
  ideology = fascism_ideology
}
create_country_leader = {
  name = "馬鴻逵"  # Ma Hongkui
  desc = "POLITICS_MA_HONGKUI_DESC"
  picture = "Portrait_Xibei_San_Ma_Ma_Hongkui.dds"
  expire = "1970.1.20"
  ideology = conservatism
}
create_country_leader = {
  name = "馬鴻逵"  # Ma Hongkui
  desc = "POLITICS_MA_HONGKUI_DESC"
  picture = "Portrait_Xibei_San_Ma_Ma_Hongkui.dds"
  expire = "1970.1.20"
  ideology = n_socialism_ideology
}
create_corps_commander = {
  name = "馬鴻逵"  # Ma Hongkui
 #desc = "1892年3月9日生於甘肅省臨夏縣，1970年1月14日於洛杉磯病逝，蘭州陸軍軍校畢業，1936年1月22日敘任陸軍中將，1936年9月26日加上將銜"
  portrait_path = "gfx/leaders/NXM/Portrait_Xibei_San_Ma_Ma_Hongkui.dds"
  traits = { career_officer }
  id = 3002
  skill = 2
  attack_skill = 2
  defense_skill = 2
  planning_skill = 2
  logistics_skill = 2
}
create_corps_commander = {
  name = "馬鴻賓"  # Ma Hongbin
 #desc = "1884年9月14日生於甘肅省臨夏縣，1960年10月21日於蘭州病逝，1936年9月14日敘任陸軍中將" 
  portrait_path = "gfx/leaders/MON/Portrait_Ma_Hongbin.dds"
  traits = {}
  id = 4004
  skill = 2
  attack_skill = 2
  defense_skill = 2
  planning_skill = 2
  logistics_skill = 2
}