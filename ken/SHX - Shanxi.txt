﻿capital = 615
oob = "SHX_1936"
set_technology = {
  infantry_weapons = 1
  gw_artillery = 1
  gwtank = 1
  early_fighter = 1
  early_bomber = 1
  mass_assault = 1
}
add_war_support = 0.15
add_stability = 0.25
set_cosmetic_tag = SHX_CLQ
1939.1.1 = {
  drop_cosmetic_tag = yes
  add_to_war = {
    targeted_alliance = CHI
    enemy = JAP
  }
  add_political_power = 1198
  if = {
    limit = {
      NOT = {
        has_dlc = "Waking the Tiger"
      }
    }
    complete_national_focus = army_effort
    complete_national_focus = equipment_effort
    complete_national_focus = motorization_effort
    complete_national_focus = aviation_effort
    complete_national_focus = construction_effort_2
    complete_national_focus = production_effort_2
    complete_national_focus = infrastructure_effort
    complete_national_focus = industrial_effort
    complete_national_focus = construction_effort
    complete_national_focus = production_effort
  }
  add_ideas = {
    tot_economic_mobilisation
    service_by_requirement
    closed_economy
  }
  set_technology = {
    air_superiority = 1
    mass_assault = 1
    pocket_defence = 1
    defence_in_depth = 1
    fleet_in_being = 1
    battlefleet_concentration = 1
    convoy_sailing = 1
    electronic_mechanical_engineering = 1
    radio = 1
    radio_detection = 1
    mechanical_computing = 1
    computing_machine = 1
    basic_encryption = 1
    basic_decryption = 1
    basic_machine_tools = 1
    improved_machine_tools = 1
    advanced_machine_tools = 1
    fuel_silos = 1
    construction1 = 1
    construction2 = 1
    construction3 = 1
    concentrated_industry = 1
    concentrated_industry2 = 1
    concentrated_industry3 = 1
  }
  oob = "SHX_1939"
  set_popularities = {
    democratic = 0
    fascism = 0
    communism = 0
    neutrality = 100
  }
  set_politics = {
    ruling_party = neutrality
    last_election = "1936.1.1"
    election_frequency = 48
    elections_allowed = no
  }
}
set_popularities = {
  democratic = 0
  fascism = 0
  communism = 0
  neutrality = 100
}
set_politics = {
  ruling_party = neutrality
  last_election = "1936.1.1"
  election_frequency = 48
  elections_allowed = no
}
create_country_leader = {
  name = "閻錫山"  # Yan Xishan
  desc = "POLITICS_YAN_XISHAN_DESC"
  picture = "Portrait_Shanxi_Yan_Xishan.dds"
  expire = "1960.7.22"
  ideology = despotism
  traits = {
    xishan_doctrine
  }
}
create_country_leader = {
  name = "閻錫山"  # Yan Xishan
  desc = "POLITICS_YAN_XISHAN_DESC"
  picture = "Portrait_Shanxi_Yan_Xishan.dds"
  expire = "1970.11.3"
  ideology = fascism_ideology
  traits = {
    xishan_doctrine
  }
}
create_country_leader = {
  name = "閻錫山"  # Yan Xishan
  desc = "POLITICS_YAN_XISHAN_DESC"
  picture = "Portrait_Shanxi_Yan_Xishan.dds"
  expire = "1970.11.3"
  ideology = conservatism
  traits = {
    xishan_doctrine
  }
}
create_country_leader = {
  name = "閻錫山"  # Yan Xishan
  desc = "POLITICS_YAN_XISHAN_DESC"
  picture = "Portrait_Shanxi_Yan_Xishan.dds"
  expire = "1970.11.3"
  ideology = n_socialism_ideology
  traits = {
    xishan_doctrine
  }
}
create_country_leader = {
  name = "閻錫山"  # Yan Xishan
  desc = "POLITICS_YAN_XISHAN_DESC"
  picture = "Portrait_Shanxi_Yan_Xishan.dds"
  expire = "1970.11.3"
  ideology = marxism
  traits = {
    xishan_doctrine
  }
}
create_field_marshal = {
  name = "閻錫山"  # Yan Xishan
 #desc = "1883年10月8日生於山西省五台縣，1960年5月23日於台北病逝，日本陸軍士官學校第21期畢業，1935年4月2日授一級上將"
  portrait_path = "gfx/leaders/SHX/Portrait_Shanxi_Yan_Xishan.dds"
  traits = { old_guard logistics_wizard }
  skill = 3
  id = 4003
  attack_skill = 2
  defense_skill = 2
  planning_skill = 3
  logistics_skill = 4
}
create_field_marshal = {
  name = "傅作義"  # Fu Zuoyi
 #desc = "1895年6月27日生於山西省榮河縣，1974年4月19日於北京病逝，保定軍校第5期畢業，1935年4月2日授二級上將"
  portrait_path = "gfx/leaders/PLM/Portrait_Mengkukuo_Fu_Zuoyi.dds"
  traits = { infantry_officer defensive_doctrine }
  skill = 4
  id = 4002
  attack_skill = 3
  defense_skill = 4
  planning_skill = 3
  logistics_skill = 3
}
create_corps_commander = {
  name = "楊愛源"  # Yang Aiyuan
 #desc = "1887年生於山西省五台縣，1959年1月2日於台北病逝，保定軍校第1期畢業，1935年12月10日授二級上將"
  portrait_path = "gfx/leaders/SHX/Portrait_Shanxi_Yang_Aiyuan.dds"
  traits = { politically_connected }
  skill = 3
  attack_skill = 2
  defense_skill = 3
  planning_skill = 3
  logistics_skill = 3
}
create_corps_commander = {
  name = "孫楚"  # Sun Chu
 #desc = "1890年生於山西省解縣，1962年1月18日於太原病逝，保定軍校第1期畢業，1935年4月8日授陸軍中將"
  portrait_path = "gfx/leaders/SHX/Shanxi_Sun_Chu.dds"
  traits = {}
  skill = 2
  attack_skill = 2
  defense_skill = 3
  planning_skill = 2
  logistics_skill = 3
}
create_corps_commander = {
  name = "王靖國" #中將 1935.4.8
 #desc = "1893年生於山西省五台縣，1952年於戰犯管理所內病逝，保定軍校第5期畢業，1935年4月8日授陸軍中將"
  portrait_path = "gfx/leaders/SHX/Portrait_Shanxi_Wang_Jingguo.dds"
  traits = {}
  skill = 2
  attack_skill = 2
  defense_skill = 3
  planning_skill = 2
  logistics_skill = 2
}
create_corps_commander = {
  name = "趙承綬"  # Zhao Chengshou
 #desc = "1891年生於山西省五台縣，1966年於北京病逝，保定軍校第5期騎兵科畢業，1936年1月24日授陸軍中將"
  portrait_path = "gfx/leaders/SHX/Portrait_Shanxi_Zhao_Chengshou.dds"
  traits = { cavalry_officer }
  skill = 2
  attack_skill = 2
  defense_skill = 3
  planning_skill = 2
  logistics_skill = 2
}
create_corps_commander = {
  name = "陳長捷" # Chen Changjie
 #desc = "1892年6月2日生於福建省閩縣，1968年4月7日文革期間於上海自殺，保定軍校第期騎科第一名畢業，1935年4月19日授陸軍中將，1937年11月15日晉任陸軍中將"
  portrait_path = "gfx/leaders/SHX/Portrait_Shanxi_Chen_Changjie.dds"
  traits = {}
  skill = 2
  attack_skill = 2
  defense_skill = 3
  planning_skill = 2
  logistics_skill = 1
}
#create_corps_commander = {
#  name = "周玳" # Zhou Dai 中將 1936.1.24
#  portrait_path = ""
#  traits = { old_guard bearer_of_artillery }
#  skill = 2
#  attack_skill = 2
#  defense_skill = 2
#  planning_skill = 2
#  logistics_skill = 1
#}
#create_corps_commander = {
#  name = "楚溪春" # Chu Xichun 少將 1936.8.26/中將 1937.5.6
#  portrait_path = ""
#  traits = {}
#  skill = 2
#  attack_skill = 2
#  defense_skill = 2
#  planning_skill = 2
#  logistics_skill = 1
#}