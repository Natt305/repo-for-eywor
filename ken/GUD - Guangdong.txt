﻿capital = 592
oob = "GUD_1936"
set_research_slots = 2
set_technology = {
  infantry_weapons = 1
}
set_popularities = {
  democratic = 0
  fascism = 0
  communism = 0
  neutrality = 100
}
set_politics = {
  ruling_party = neutrality
  last_election = "1936.1.1"
  election_frequency = 48
  elections_allowed = no
}
create_country_leader = {
  name = "陳濟棠"  # Chen Jitang
  desc = "POLITICS_CHEN_JITANG_DESC"
  picture = "Portrait_Guangdong_Chen_Jitang.dds"
  expire = "1954.11.3"
  ideology = despotism
}
create_country_leader = {
  name = "陳濟棠"  # Chen Jitang
  desc = "POLITICS_CHEN_JITANG_DESC"
  picture = "Portrait_Guangdong_Chen_Jitang.dds"
  expire = "1954.11.3"
  ideology = fascism_ideology
}
create_country_leader = {
  name = "陳濟棠"  # Chen Jitang
  desc = "POLITICS_CHEN_JITANG_DESC"
  picture = "Portrait_Guangdong_Chen_Jitang.dds"
  expire = "1954.11.3"
  ideology = conservatism
}
create_country_leader = {
  name = "陳濟棠"  # Chen Jitang
  desc = "POLITICS_CHEN_JITANG_DESC"
  picture = "Portrait_Guangdong_Chen_Jitang.dds"
  expire = "1954.11.3"
  ideology = n_socialism_ideology
}
create_country_leader = {
  name = "陳濟棠"  # Chen Jitang
  desc = "POLITICS_CHEN_JITANG_DESC"
  picture = "Portrait_Guangdong_Chen_Jitang.dds"
  expire = "1954.11.3"
  ideology = marxism
}
create_country_leader = {
  name = "陳濟棠"  # Chen Jitang
  desc = "POLITICS_CHEN_JITANG_DESC"
  picture = "Portrait_Guangdong_Chen_Jitang.dds"
  expire = "1954.11.3"
  ideology = n_socialism_ideology
}
create_field_marshal = {
  name = "陳濟棠"  # Chen Jitang
 #desc = "1890年1月23日生於廣東省防城縣，1954年11月3日於台北病逝，廣東陸軍速成學校畢業，1935年4月2日敘任陸軍一級上將"  
  portrait_path = "gfx/leaders/GUD/Portrait_Guangdong_Chen_Jitang.dds"
  traits = { logistics_wizard }
  id = 2503
  skill = 3
  attack_skill = 2
  defense_skill = 2
  planning_skill = 3
  logistics_skill = 3
}
create_corps_commander = {
  name = "余漢謀"  # Yu Hanmou
 #desc = "1896年9月22日生於廣東省高要縣，1981年12月27日於台北病逝，保定軍校第6期畢業，1936年1月22日敘任陸軍陸軍中將，1936年9月12日加上將銜，1946年6月13日晉任陸軍二級上將"
  portrait_path = "gfx/leaders/GUD/Portrait_Guangdong_Yu_Hanmou.dds"
  traits = { career_officer infantry_officer }
  skill = 3
  attack_skill = 2
  defense_skill = 2
  planning_skill = 3
  logistics_skill = 3
}
create_corps_commander = {
  name = "李漢魂"  # Li Hanhun
 #desc = "1895年11月23日生於廣東省吳川縣，1987年6月30日於紐約病逝，保定軍校第6期畢業，1936年1月25日敘任陸軍陸軍中將，1949年2月21日加上將銜"
  portrait_path = "gfx/leaders/CHI/Portrait_Li_hanhun.dds"
  traits = {}
  skill = 2
  attack_skill = 2
  defense_skill = 3
  planning_skill = 2
  logistics_skill = 3
}
create_corps_commander = {
  name = "鄧龍光"  # Deng Longguang
 #desc = "1896年11月22日生於廣東省茂名縣，1979年2月3日於台北病逝，保定軍校第6期畢業，1936年1月25日敘任陸軍陸軍中將"
  portrait_path = "gfx/leaders/GUD/Portrait_Guangdong_Deng_Longguang.dds"
  traits = {}
  skill = 2
  attack_skill = 2
  defense_skill = 3
  planning_skill = 2
  logistics_skill = 2
}
#create_corps_commander = {
#  name = "葉肇" # Ye Zhao
# #desc = "1892年生於廣東省新興縣，1953年2月7日於台北病逝，保定軍校第6期畢業，1936年1月29日敘任陸軍陸軍少將，1936年10月5日晉任陸軍中將"
#  portrait_path = ""
#  traits = {}
#  skill = 2
#  attack_skill = 2
#  defense_skill = 2
#  planning_skill = 2
#  logistics_skill = 1
#}