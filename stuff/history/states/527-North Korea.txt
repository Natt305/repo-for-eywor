
state={
	id=527
	name="STATE_527"
	resources={
		steel=20.000
		tungsten=32.000
	}

	history={
		owner = JAP
		buildings = {
			infrastructure = 5
			air_base = 6
			industrial_complex = 1
		}
		victory_points = {
			4052 5 
		}
		add_core_of = KOR
	}

	provinces={
		912 978 3912 4052 6963 7171 9790 9795 9918 10065 10083 11770 11775 11828 11835 11896 
	}
	manpower=4114200
	buildings_max_level_factor=1.000
	state_category=large_town
}
