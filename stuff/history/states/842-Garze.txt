
state={
	id=842
	name="STATE_842"
	resources={
		steel=5.000
	}

	history={
		owner = XIK
		add_core_of = XIK
		add_core_of = SIC
		add_core_of = CHI
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			arms_factory = 1

		}
		1939.1.1 = {
			buildings = {
				industrial_complex = 1
				infrastructure = 2

			}

		}

	}

	provinces={
		1999 8104 12724 12837 
	}
	manpower=368200
	buildings_max_level_factor=1.000
	state_category=rural
}
