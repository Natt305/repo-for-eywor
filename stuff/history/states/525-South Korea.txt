
state={
	id=525
	name="STATE_525"
	resources={
		steel=6.000
	}

	history={
		owner = JAP
		buildings = {
			infrastructure = 6
			arms_factory = 1
			air_base = 4
		}
		victory_points = {
			7125 5 
		}
		add_core_of = KOR
	}

	provinces={
		1100 1148 7021 7125 7221 9936 10036 12011 
	}
	manpower=7074105
	buildings_max_level_factor=1.000
	state_category=large_town
}
