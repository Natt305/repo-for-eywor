﻿capital = 759

oob = "TNG_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
	mass_assault = 1
}
add_opinion_modifier = {
	target = PRC
	modifier = hostile_status
}
add_ideas = MON_nomadic
1939.1.1 = {

	add_political_power = 1198

	if = {
		limit = {
			NOT = {
				has_dlc = "Waking the Tiger"
			}
		}
		complete_national_focus = army_effort
		complete_national_focus = equipment_effort
		complete_national_focus = motorization_effort
		complete_national_focus = aviation_effort
		complete_national_focus = construction_effort_2
		complete_national_focus = production_effort_2
		complete_national_focus = infrastructure_effort
		complete_national_focus = industrial_effort
		complete_national_focus = construction_effort
		complete_national_focus = production_effort
	}

	add_ideas = {
		#laws
		tot_economic_mobilisation
		service_by_requirement
		closed_economy
	}

	oob = "TNG_1939"

	set_technology = {
		#doctrines
		air_superiority = 1
		pocket_defence = 1
		defence_in_depth = 1
		fleet_in_being = 1
		battlefleet_concentration = 1
		convoy_sailing = 1

		#electronics
		electronic_mechanical_engineering = 1
		radio = 1
		radio_detection = 1
		mechanical_computing = 1
		computing_machine = 1
		basic_encryption = 1
		basic_decryption = 1

		#industry
		basic_machine_tools = 1
		improved_machine_tools = 1
		advanced_machine_tools = 1
		synth_oil_experiments = 1
		oil_processing = 1
		improved_oil_processing = 1
		construction1 = 1
		construction2 = 1
		construction3 = 1
		concentrated_industry = 1
		concentrated_industry2 = 1
		concentrated_industry3 = 1
	}

	set_popularities = {
		democratic = 0
		fascism = 0
		communism = 0
		neutrality = 100

	}
	set_politics = {

		ruling_party = neutrality
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
}

set_popularities = {
	democratic = 0
	fascism = 0
	communism = 0
	neutrality = 100

}
set_politics = {
	ruling_party = neutrality
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

create_country_leader = {
	name = "Ma Hushan"
	desc = "POLITICS_MA_HUSHAN_DESC"
	picture = "Portrait_Ma_Hushan.dds"
	expire = "1975.7.1"
	ideology = despotism
	traits = {
		#
	}
}


create_country_leader = {
  name = "Ma Hushan"
	desc = "POLITICS_MA_HUSHAN_DESC"
	picture = "Portrait_Ma_Hushan.dds"
	expire = "1970.11.3"
	ideology = fascism_ideology
}

create_country_leader = {
  name = "Ma Hushan"
	desc = "POLITICS_MA_HUSHAN_DESC"
	picture = "Portrait_Ma_Hushan.dds"
	expire = "1970.11.3"
	ideology = conservatism
}

create_country_leader = {
  name = "Ma Hushan"
	desc = "POLITICS_MA_HUSHAN_DESC"
	picture = "Portrait_Ma_Hushan.dds"
	expire = "1970.11.3"
	ideology = n_socialism_ideology
}

create_country_leader = {
 	name = "Ma Hushan"
	desc = "POLITICS_MA_HUSHAN_DESC"
	picture = "Portrait_Ma_Hushan.dds"
	expire = "1970.11.3"
	ideology = marxism
}

create_field_marshal = {
	name = "Ma Hushan"
	picture = "Portrait_Ma_Hushan.dds"
	traits = { defensive_doctrine }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}
