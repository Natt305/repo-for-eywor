﻿capital = 1

every_country = {
	limit = {
		OR = {
			capital_scope = {
				OR = {
					is_on_continent = europe
					is_on_continent = north_america
					is_on_continent = oceania
				}
			}
			is_subject_of = ENG
			is_subject_of = HOL
			is_subject_of = FRA
			tag = FIC
		}
	}

	add_ideas = {
		generic_wartime_propagantist_europe
		generic_staunch_patriot_europe
		generic_progressive_activist_europe
		generic_revolutionary_syndicalist_europe
		generic_robber_baron_europe
	}
	set_country_flag = european_country_flag
	remove_ideas = {
		generic_wartime_propagantist_europe
		generic_staunch_patriot_europe
		generic_progressive_activist_europe
		generic_revolutionary_syndicalist_europe
		generic_robber_baron_europe
	}

}
TAN = {
	set_cosmetic_tag = TAN_RED	
}
MON = {
	set_cosmetic_tag = MON_RED
}
every_country = {
	limit = {
		OR = {
			AND = {
				capital_scope = { is_on_continent = asia }
				is_puppet = no
			}
			is_subject_of = JAP
			is_warlord = yes
			tag = HPC
			tag = GUD
			tag = XIK
			tag = SIC
			tag = EHB
			tag = ECA
			tag = RFM
			tag = MAL
			tag = SAR
			tag = PLM
			tag = NEA
			tag = SCG
			tag = NSG
		}
	}
	add_ideas = {
		generic_wartime_propagantist_asia
		generic_staunch_patriot_asia
		generic_progressive_activist_asia
		generic_revolutionary_syndicalist_asia
		generic_robber_baron_asia
	}
	set_country_flag = asian_country_flag
	remove_ideas = {
		generic_wartime_propagantist_asia
		generic_staunch_patriot_asia
		generic_progressive_activist_asia
		generic_revolutionary_syndicalist_asia
		generic_robber_baron_asia
	}

}
every_country = {
	limit = {
		capital_scope = { is_on_continent = middle_east }
		is_puppet = no
	}
	add_ideas = {
		generic_wartime_propagantist_arab
		generic_staunch_patriot_arab
		generic_progressive_activist_arab
		generic_revolutionary_syndicalist_arab
		generic_robber_baron_arab
	}
	set_country_flag = arab_country_flag
	remove_ideas = {
		generic_wartime_propagantist_arab
		generic_staunch_patriot_arab
		generic_progressive_activist_arab
		generic_revolutionary_syndicalist_arab
		generic_robber_baron_arab
	}
}
every_country = {
	limit = {
		AND = {
			capital_scope = { is_on_continent = africa }
			is_puppet = no
		}
	}
	add_ideas = {
		generic_wartime_propagantist_africa
		generic_staunch_patriot_africa
		generic_progressive_activist_africa
		generic_revolutionary_syndicalist_africa
		generic_robber_baron_africa
	}
	set_country_flag = africa_country_flag
	remove_ideas = {
		generic_wartime_propagantist_africa
		generic_staunch_patriot_africa
		generic_progressive_activist_africa
		generic_revolutionary_syndicalist_africa
		generic_robber_baron_africa
	}
}
every_country = {
	limit = {
		OR = {
			AND = {
				capital_scope = { is_on_continent = south_america }
				is_puppet = no
			}
			tag = PHI
		}
	}
	add_ideas = {
		generic_wartime_propagantist_south_america
		generic_staunch_patriot_south_america
		generic_progressive_activist_south_america
		generic_revolutionary_syndicalist_south_america
		generic_robber_baron_south_america
	}
	set_country_flag = south_america_country_flag
	remove_ideas = {
		generic_wartime_propagantist_south_america
		generic_staunch_patriot_south_america
		generic_progressive_activist_south_america
		generic_revolutionary_syndicalist_south_america
		generic_robber_baron_south_america
	}
}
1939.1.1 = {
	every_country = {
		limit = {
			OR = {
				capital_scope = {
					OR = {
						is_on_continent = europe
						is_on_continent = north_america
						is_on_continent = oceania
					}
				}
				is_subject_of = ENG
				is_subject_of = HOL
				is_subject_of = FRA
				tag = FIC
			}
		}

		add_ideas = {
			generic_wartime_propagantist_europe
			generic_staunch_patriot_europe
			generic_progressive_activist_europe
			generic_revolutionary_syndicalist_europe
			generic_robber_baron_europe
		}
		set_country_flag = european_country_flag
		remove_ideas = {
			generic_wartime_propagantist_europe
			generic_staunch_patriot_europe
			generic_progressive_activist_europe
			generic_revolutionary_syndicalist_europe
			generic_robber_baron_europe
		}

	}
	every_country = {
		limit = {
			OR = {
				AND = {
					capital_scope = { is_on_continent = asia }
					is_puppet = no
				}
				is_subject_of = JAP
				is_warlord = yes
				tag = HPC
				tag = GUD
				tag = XIK
				tag = SIC
				tag = EHB
				tag = ECA
				tag = RFM
				tag = PLM
				tag = NEA
				tag = SCG
				tag = NSG
			}
		}
		add_ideas = {
			generic_wartime_propagantist_asia
			generic_staunch_patriot_asia
			generic_progressive_activist_asia
			generic_revolutionary_syndicalist_asia
			generic_robber_baron_asia
		}
		set_country_flag = asian_country_flag
		remove_ideas = {
			generic_wartime_propagantist_asia
			generic_staunch_patriot_asia
			generic_progressive_activist_asia
			generic_revolutionary_syndicalist_asia
			generic_robber_baron_asia
		}

	}
	every_country = {
		limit = {
			capital_scope = { is_on_continent = middle_east }
			is_puppet = no
		}
		add_ideas = {
			generic_wartime_propagantist_arab
			generic_staunch_patriot_arab
			generic_progressive_activist_arab
			generic_revolutionary_syndicalist_arab
			generic_robber_baron_arab
		}
		set_country_flag = arab_country_flag
		remove_ideas = {
			generic_wartime_propagantist_arab
			generic_staunch_patriot_arab
			generic_progressive_activist_arab
			generic_revolutionary_syndicalist_arab
			generic_robber_baron_arab
		}
	}
	every_country = {
		limit = {
			AND = {
				capital_scope = { is_on_continent = africa }
				is_puppet = no
			}
		}
		add_ideas = {
			generic_wartime_propagantist_africa
			generic_staunch_patriot_africa
			generic_progressive_activist_africa
			generic_revolutionary_syndicalist_africa
			generic_robber_baron_africa
		}
		set_country_flag = africa_country_flag
		remove_ideas = {
			generic_wartime_propagantist_africa
			generic_staunch_patriot_africa
			generic_progressive_activist_africa
			generic_revolutionary_syndicalist_africa
			generic_robber_baron_africa
		}
	}
	every_country = {
		limit = {
			OR = {
				AND = {
					capital_scope = { is_on_continent = south_america }
					is_puppet = no
				}
				tag = PHI
			}
		}
		add_ideas = {
			generic_wartime_propagantist_south_america
			generic_staunch_patriot_south_america
			generic_progressive_activist_south_america
			generic_revolutionary_syndicalist_south_america
			generic_robber_baron_south_america
		}
		set_country_flag = south_america_country_flag
		remove_ideas = {
			generic_wartime_propagantist_south_america
			generic_staunch_patriot_south_america
			generic_progressive_activist_south_america
			generic_revolutionary_syndicalist_south_america
			generic_robber_baron_south_america
		}
	}
}

########THIS SECTION IS FOR NON ASIAN COUNTRIE'S HISTORY
NEP = {
	create_country_leader = {
		name = "Juddha Rana"
		desc = "POLITICS_JUDDHA_RANA_DESC"
		picture = "gfx/leaders/NEP/Portrait_Nepal_Juddha_Rana.dds"
		expire = "1965.1.1"
		ideology = despotism
		traits = {
			#
		}
	}
}

NOR = {
	set_popularities = {
		democratic = 44
		n_socialism = 52
		fascism = 2
		communism = 2
		neutrality = 0
	}
	set_politics = {

		ruling_party = n_socialism
		last_election = "1933.10.16"
		election_frequency = 36
		elections_allowed = yes
	}



	create_country_leader = {
		name = "Johan Nygaardsvold"
		desc = "POLITICS_JOHAN_NYGAARDSVOLD_DESC"
		picture = "Portrait_Norway_Johan_Nygaardsvold.dds"
		expire = "1965.1.1"
		ideology = n_socialism_ideology
		traits = {
			#
		}
	}
	create_country_leader = {
		name = "Johan Ludwig Mowinckel"
		desc = "POLITICS_JOHAN_LUDWIG_MOWINCKEL_DESC"
		picture = "gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
		expire = "1965.1.1"
		ideology = liberalism
		traits = {
			#
		}
	}
}
RAJ = {
	remove_ideas = RAJ_agrarian_society
}

TAN = {
	create_country_leader = {
		name = "Salchak Toka"
		desc = "POLITICS_SALCHAK_TOKA_DESC"
		picture = "gfx/leaders/TAN/Portrait_Tannu_Tuva_Salchak_Toka.dds"
		expire = "1965.1.1"
		ideology = stalinism
		traits = {
			#
		}
	}
}

DEN = {
	set_popularities = {
		democratic = 42
		n_socialism = 55
		fascism = 1
		communism = 2
		neutrality = 0
	}
	set_politics = {

		ruling_party = n_socialism
		last_election = "1935.10.22"
		election_frequency = 48
		elections_allowed = yes
	}





	create_country_leader = {
		name = "Thorvald Stauning"
		desc = "POLITICS_THORVALD_STAUNING_DESC"
		picture = "Portrait_Denmark_Thorvald_Stauning.dds"
		expire = "1965.1.1"
		ideology = n_socialism_ideology
		traits = {
			#
		}
	}
	create_country_leader = {
		name = "Thomas Madsen-Mygdal"
		desc = ""
		picture = "Portrait_Denmark_Thomas_Madsen-Mygdal.dds"
		expire = "1965.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}
}

BEL = {
	create_country_leader = {
		name = "Henri de Man"
		desc = "POLITICS_HENRI_DE_MAN_DESC"
		picture = "Portrait_Belgium_Henri_De_Man.dds"
		expire = "1965.1.1"
		ideology = n_socialism_ideology
		traits = {
			#
		}
	}
}

NZL = {
	set_popularities = {
		democratic = 45
		fascism = 0
		n_socialism = 55
		communism = 0
		neutrality = 0
	}
	set_politics = {

		ruling_party = n_socialism
		last_election = "1935.11.27"
		election_frequency = 36
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Michael Joseph Savage"
		desc = "POLITICS_MICHAEL_JOSEPH_SAVAGE_DESC"
		picture = "GFX_NZL_michael_joseph_savage"
		expire = "1965.1.1"
		ideology = n_socialism_ideology
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Sidney Holland"
		desc = "POLITICS_SIDNEY_HOLLAND_DESC"
		picture = "GFX_NZL_sidney_holland"
		expire = "1965.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}




}
AST = {
	set_popularities = {
		democratic = 40
		n_socialism = 58
		fascism = 0
		communism = 2
		neutrality = 0
	}
	set_politics = {

		ruling_party = n_socialism
		last_election = "1934.9.15"
		election_frequency = 36
		elections_allowed = yes
	}

	create_country_leader = {
		name = "John Curtin"
		desc = "POLITICS_JOHN_CURTIN_DESC"
		picture = "Portrait_Australia_John_Curtin.dds"
		expire = "1965.1.1"
		ideology = n_socialism_ideology
		traits = {
			#
		}
	}

	create_country_leader = {
		name = "Joseph Lyons"
		desc = ""
		picture = "GFX_AST_earle_page"
		expire = "1965.1.1"
		ideology = conservatism
		traits = {
			#
		}
	}
	create_country_leader = {
		name = "Joseph Lyons"
		desc = "POLITICS_EARLE_PAGE_DESC"
		picture = "Portrait_Australia_Joseph_Lyons.dds"
		expire = "1965.1.1"
		ideology = centrism
		traits = {
			#
		}
	}
}
FRA = {
	set_popularities = {
		democratic = 39
		fascism = 1
		n_socialism = 40
		communism = 20
		neutrality = 0
	}
	set_politics = {

		ruling_party = democratic
		last_election = "1932.5.1"
		election_frequency = 48
		elections_allowed = yes
	}
	create_country_leader = {
		name = "Léon Blum"
		desc = ""
		picture = "Portrait_Leon_Blum.dds"
		expire = "1965.1.1"
		ideology = n_socialism_ideology
		traits = {

		}
	}

	create_country_leader = {
		name = "Pierre Laval"
		desc = ""
		picture = "Portrait_Pierre_Laval.dds"
		expire = "1965.1.1"
		ideology = conservatism
		traits = {

		}
	}

	create_country_leader = {
		name = "Philippe Pétain"
		desc = "POLITICS_PHILIPPE_PÉTAIN_DESC"
		picture = "Portrait_France_Philippe_Petain.dds"
		expire = "1965.1.1"
		ideology = collaborationism
		traits = {

		}
	}
}
ENG = {
	set_autonomy = {
		target = RAJ
		autonomous_state = autonomy_colony
	}
}
RAJ = {
	set_popularities = {
		democratic = 17
		fascism = 2
		communism = 1
		neutrality = 80
		n_socialism = 0
	}
	set_politics = {

		ruling_party = neutrality
		last_election = "1935.11.14"
		election_frequency = 48
		elections_allowed = no
	}
}

ENG = {
	if = {
		limit = {
			has_dlc = "Together for Victory"
		}
		set_autonomy = {
			target = SST
			autonomous_state = autonomy_integrated_puppet
			freedom_level = 0.6
		}
		else = {
			puppet = SST
		}
	}
}

SST = {
	set_popularities = {
		democratic = 30
		fascism = 0
		n_socialism = 0
		communism = 0
		neutrality = 70
	}
	set_politics = {

		ruling_party = neutrality
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
}

ENG = {
	if = {
		limit = {
			has_dlc = "Together for Victory"
		}
		set_autonomy = {
			target = MAL
			autonomous_state = autonomy_integrated_puppet
		}
		else = {
			puppet = MAL
		}
	}
}

MAL = {
	set_popularities = {
		democratic = 20
		fascism = 0
		n_socialism = 17
		communism = 13
		neutrality = 50
	}
	set_politics = {

		ruling_party = neutrality
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
	}
}

HOL = {
	if = {
		limit = {
			has_dlc = "Together for Victory"
		}
		set_autonomy = {
			target = INS
			autonomous_state = autonomy_colony
			freedom_level = 0.2
		}
		else = {
			puppet = INS
		}
	}

	set_popularities = {
		democratic = 66
		fascism = 1
		communism = 5
		n_socialism = 28
		neutrality = 0
	}
	set_politics = {

		ruling_party = democratic
		last_election = "1933.4.26"
		election_frequency = 48
		elections_allowed = yes
	}

	create_country_leader = {
		name = "Johann Willem Albarda"
		desc = "POLITICS_JOHANN_ALBARDA_DESC"
		picture = "Portrait_Netherlands_Dirk_Jan_de_Geer.dds"
		expire = "1965.1.1"
		ideology = n_socialism_ideology
		traits = {
			#
		}
	}
}
INS = {
	set_popularities = {
		democratic = 27
		n_socialism = 7
		fascism = 1
		communism = 3
		neutrality = 62
	}
	set_politics = {
		ruling_party = neutrality
		last_election = "1935.11.14"
		election_frequency = 48
		elections_allowed = yes
	}

}

########1939.1.1
1939.1.1 = {
	#INS = {
	#	set_technology = {
	#		#doctrines
	#		improved_light_cruiser = 1
	#
	#		#air
	#		fighter1 = 1
	#	}
	#	create_country_leader = {
	#		name = "Mohammad Husni Thamrin"
	#		desc = ""
	#		picture = "gfx/leaders/INS/Portrait_Indonesia_Mohammad_Husni_Thamrin.dds"
	#		expire = "1953.3.1"
	#		ideology = conservatism
	#		traits = {
	#
	#		}
	#	}
	#	create_country_leader = {
	#		name = "A. T. van Starkenborgh Stachouwer"
	#		desc = ""
	#		picture = "gfx/leaders/INS/Portrait_Alidius_van_Starkenborgh_Stachouwer.dds"
	#		expire = "1953.3.1"
	#		ideology = despotism
	#		traits = {
	#
	#		}
	#	}
	#}
	NOR = {
		set_popularities = {
			democratic = 36
			fascism = 2
			n_socialism = 52
			communism = 2
			neutrality = 8
		}
		set_politics = {

			ruling_party = n_socialism
			last_election = "1936.10.19"
			election_frequency = 36
			elections_allowed = yes
		}
	}
	NZL = {
		set_popularities = {
			democratic = 45
			fascism = 0
			n_socialism = 55
			communism = 0
			neutrality = 0
		}
		set_politics = {

			ruling_party = democratic
			last_election = "1938.10.15"
			election_frequency = 60
			elections_allowed = yes
		}

		# Didn't take office until '40, added for flavor
		create_country_leader = {
			name = "Peter Fraser"
			desc = "POLITICS_PETER_FRASER_DESC"
			picture = "Portrait_NewZealand_Peter_Fraser.dds"
			expire = "1965.1.1"
			ideology = n_socialism_ideology
			traits = {
				#
			}
		}

	}
	DEN = {
		set_popularities = {
			democratic = 42
			n_socialism = 55
			fascism = 1
			communism = 2
			neutrality = 0
		}
		set_politics = {

			ruling_party = n_socialism
			last_election = "1939.4.3"
			election_frequency = 48
			elections_allowed = yes
		}
	}
	ENG = {
		if = {
			limit = {
				has_dlc = "Together for Victory"
			}
			set_autonomy = {
				target = BUR
				autonomous_state = autonomy_colony
				freedom_level = 0.35
			}
			else = {
				puppet = BUR
			}
		}
	}
	FRA = {
		remove_ideas = FRA_front_populaire
		set_politics = { ruling_party = democratic elections_allowed = yes }
		create_country_leader = {
			name = "Édouard Daladier"
			desc = ""
			picture = "Portrait_France_Edouard_Daladier.dds"
			expire = "1965.1.1"
			ideology = socialism
			traits = {

			}
		}
	}
	HOL = {
		set_popularities = {
			democratic = 63
			fascism = 4
			communism = 3
			n_socialism = 30
			neutrality = 0
		}
		set_politics = {

			ruling_party = democratic
			last_election = "1937.5.26"
			election_frequency = 48
			elections_allowed = yes
		}
	}
	every_country = {
		limit = {
			is_warlord = yes
		}
		if = {
			limit = {
				has_dlc = "Waking the Tiger"
			}
			CHI = { add_to_faction = PREV }
			add_to_war = {
				targeted_alliance = CHI
				enemy = JAP
				hostility_reason = asked_to_join
			}

			complete_national_focus = CHI_secure_internal_politics
			if = {
				limit = {
					tag = GXC
				}
				unlock_national_focus = CHI_opposition
				else = {
					complete_national_focus = CHI_cooperation_with_the_nationalists
				}
			}
			complete_national_focus = CHI_technological_cooperation
			complete_national_focus = CHI_root_out_corruption
			complete_national_focus = CHI_reform_the_administration
			else = {
				#generic focuses
				complete_national_focus = army_effort
				complete_national_focus = equipment_effort
				complete_national_focus = motorization_effort
				complete_national_focus = aviation_effort
				complete_national_focus = naval_effort
				complete_national_focus = flexible_navy
				complete_national_focus = industrial_effort
				complete_national_focus = construction_effort
				complete_national_focus = production_effort
			}
		}

	}
}
