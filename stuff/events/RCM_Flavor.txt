﻿###################
###FLAVOR EVENTS###
###################

add_namespace = rcmflavor


#Death of Lin Sen (ROC)
country_event = {
	id = rcmflavor.1
	title = rcmflavor.1.t
	desc = rcmflavor.1.d
	picture = GFX_report_event_lin_sen
 
	is_triggered_only = no
	fire_only_once = yes
	
	trigger = {
		tag = CHI
		date > 1943.7.20
	}
	option = {
		name = rcmflavor.1.a
		remove_ideas = CHI_lin_sen
		set_country_flag = lin_sen_dead
	}
}

#Death of Saionji Kinmochi (JAP)
country_event = {
	id = rcmflavor.2
	title = rcmflavor.2.t
	desc = rcmflavor.2.d
	picture = GFX_report_event_last_genro
 
	is_triggered_only = no
	fire_only_once = yes
	
	trigger = {
		tag = JAP
		date > 1940.11.10
	}
	option = {
		name = rcmflavor.2.a
		remove_ideas = saionji_kinmochi
		set_country_flag = saionji_kinmochi_dead
	}
}

#Death of Isoroku Yamamoto (JAP)
country_event = {
	id = rcmflavor.3
	title = rcmflavor.3.t
	desc = rcmflavor.3.d
	picture = GFX_report_event_yamamoto
 
	is_triggered_only = no
	fire_only_once = yes
	
	trigger = {
		tag = JAP
		has_war_with = USA
		NOT = {
			has_completed_focus = JAP_navy_leadership_focus
		}
		NOT = { has_country_flag = purged_by_kodoha }
		surrender_progress > 0.05
	}
	option = {
		name = rcmflavor.3.a
		add_political_power = -25
		#if = {
		#	limit = {
		#		has_country_leader = {
		#			name = "Isoroku Yamamoto" ruling_only = yes
		#		}
		#	}
		#	kill_country_leader = yes
		#}
		hidden_effect = {
			remove_ideas = isoroku_yamamoto
			set_country_flag = yamamoto_dead
		}
		remove_unit_leader = 2903
	}
}