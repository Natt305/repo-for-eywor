#	Example:
#
#	example_trigger = {
#		tag = GER
#		is_ai = no
#	}
#
#
#	In a script file:
#
#	trigger = {
#		example_trigger = yes
#	}
#
#	THIS FILE IS FOR TRIGGERS SPECIFICALLY FOR THE CHI TAG
suitable_for_sik_forming = {
	is_controlled_by_jap_or_its_puppet = yes
	is_owned_by_jap_or_its_puppet = yes
	controller = {
		has_full_control_of_state = PREV
	}
	owner = {
		OR = {
			tag = JAP
			is_rrc = yes
		}
	}
}
suitable_for_hui_hui_forming = {
	is_controlled_by_jap_or_its_puppet = yes
	is_owned_by_jap_or_its_puppet = yes
	controller = {
		has_full_control_of_state = PREV
	}
	owner = {
		OR = {
			tag = JAP
			is_rrc = yes
		}
	}
}
is_using_generic_idea = {
	NOT = {
		OR = {
			tag = GER
			tag = ENG
			tag = SOV
			tag = FRA
			tag = ITA
			tag = JAP
			tag = MAN
			tag = USA
			tag = CAN
			tag = ROM
			tag = YUG
		}
	}
	if = {
		limit = {
			has_dlc = "Waking the Tiger"
		}
		NOT = {
			OR = {
				tag = CHI
				tag = PRC
				is_originally_warlord = yes
			}
		}
	}
}
is_northeast_china_state = {
	OR = {
		state = 714
		state = 715
		state = 716
		state = 717
		state = 328
		state = 834
		state = 835
		state = 836
		state = 837
		state = 838
		state = 839
		state = 840
		state = 841
		state = 610
		state = 757
		state = 747
	}
}
is_south_china_state = {
	OR = {
		state = 325
		state = 831
		state = 832
		state = 599
		state = 845
		state = 833
		state = 592
		state = 846
		state = 593
		state = 847
		state = 603
		state = 843
		state = 602
		state = 844
		state = 600
		state = 595
		state = 824
	}
}
is_east_china_state = {
	OR = {
		state = 607
		state = 746
		state = 620
		state = 756
		state = 745
		state = 744
		state = 606
		state = 828
		state = 596
		state = 597
		state = 826
		state = 598
		state = 825
		state = 613
	}
}

is_north_china_state = {
	OR = {
		state = 608
		state = 609
		state = 611
		state = 614
		state = 621
		state = 823
		state = 615
		state = 789
		state = 827
		state = 802
		state = 622
		state = 616
		state = 760
		state = 612
		#state = 757
	}
}
is_west_china_state = {
	OR = {
		state = 604
		state = 790
		state = 791
		state = 793
		state = 794
		state = 283
		state = 792
		state = 601
		state = 842
		state = 860
		state = 605
		state = 829
		state = 830
		state = 743
		state = 761
	}
}
has_long_march_idea = {
	custom_trigger_tooltip = {
		tooltip = not_affected_by_longmarch_tt
		OR = {
			has_idea = Long_March_1
			has_idea = Long_March_2
			has_idea = Long_March_3
			has_idea = Long_March_4
			has_idea = Long_March_5
		}
	}
}

is_legit_china = {
	custom_trigger_tooltip = {
		tooltip = is_legit_china_tt
		OR = {
			tag = event_target:WTT_current_china_leader			
			AND = {
				is_puppet = no
				is_literally_china = yes
				NOT = {
					country_exists = event_target:WTT_current_china_leader
					any_neighbor_country = {				
						capital_scope = {
							is_core_of = CHI
							is_core_of = PREV
						}
					}
				}
			}
		}
		#OR = {
		#	is_warlord = yes
		#	tag = CHI
		#	tag = PRC
		#}
		#OR = {
		#	tag = event_target:WTT_current_china_leader
		#	has_country_flag = im_legit_now
		#	OR = {
		#		owns_state = 598
		#		AND = {
		#			owns_state = 605
		#			is_faction_leader = yes
		#		}
		#		OR = {
		#			598 = {
		#				owner = {
		#					is_subject_of = ROOT
		#				}
		#			}
		#			605 = {
		#				owner = {
		#					is_subject_of = ROOT
		#				}
		#			}
		#		}
		#	}
		#	AND = {
		#		tag = NEA
		#		has_war_with = CHI
		#	}
		#}
	}
}
is_independent_china_or_warlord = { #Does not include sinkiang
	OR = {
		has_country_flag = im_warlord_now
		tag = CHI
		tag = PRC
		tag = GXC
		tag = YUN
		tag = SHX
		tag = XSM
		tag = XIK
		tag = SIC
		tag = GUD
		tag = HPC
		tag = NEA
		tag = SHD
		tag = NXM
		tag = TNG
		tag = HMI
	}
	is_puppet = no
}
is_warlord = {
	custom_trigger_tooltip = {
		tooltip = is_warlord_tt
		OR = {
			has_country_flag = im_warlord_now
			tag = GXC
			tag = YUN
			tag = SHX
			tag = XSM
			tag = XIK
			tag = SIC
			tag = GUD
			tag = HPC
			tag = NEA
			tag = SHD
			tag = NXM
			tag = HMI
			tag = TNG
		}
	}
}
is_warlord_for_allowed_trigger = {
	OR = {
		original_tag = SIK
		original_tag = PLM
		original_tag = GXC
		original_tag = YUN
		original_tag = SHX
		original_tag = XSM
		original_tag = XIK
		original_tag = SIC
		original_tag = GUD
		original_tag = HPC
		original_tag = NEA
		original_tag = SHD
		original_tag = NXM
		original_tag = TNG
		original_tag = HMI
	}
}
is_warlord_for_allowed_trigger = {
	OR = {
		original_tag = SIK
		original_tag = PLM
		original_tag = GXC
		original_tag = YUN
		original_tag = SHX
		original_tag = XSM
		original_tag = XIK
		original_tag = SIC
		original_tag = GUD
		original_tag = HPC
		original_tag = NEA
		original_tag = SHD
		original_tag = NXM
		original_tag = TNG
		original_tag = HMI
	}
}
is_originally_warlord = { #Does not include sinkiang
	OR = {
		has_country_flag = im_warlord_now
		original_tag = GXC
		original_tag = YUN
		original_tag = SHX
		original_tag = XSM
		original_tag = XIK
		original_tag = SIC
		original_tag = GUD
		original_tag = HPC
		original_tag = NEA
		original_tag = SHD
		original_tag = NXM
		original_tag = TNG
		original_tag = HMI
	}
}

is_north_china_state_for_liberation = {
	OR = {
		state = 597
		state = 826
		state = 615
		state = 789
		state = 827
		state = 614
		state = 608
		state = 609
		state = 611
		state = 802
	}
}
is_MAN_state_for_liberation = {
	OR = {
		state = 714
		state = 715
		state = 716
		state = 717
		state = 328
		state = 834
		state = 835
		state = 836
		state = 837
		state = 838
		state = 839
		state = 840
		state = 841
		state = 610
		state = 747
		state = 757
		state = 612
	}
}
###

infra_ava_2_tr = {
	custom_trigger_tooltip = {
		tooltip = infra_ava_2_tt
		free_building_slots = {
			building = infrastructure
			size < 8
		}
		is_controlled_by = ROOT
	}
}
###

infra_ava_3_tr = {
	custom_trigger_tooltip = {
		tooltip = infra_ava_3_tt
		free_building_slots = {
			building = infrastructure
			size < 5
		}
		is_controlled_by = ROOT
	}
}
###

infra_bypass_1_tr = {
	custom_trigger_tooltip = {
		tooltip = infra_bypass_1_tt
		free_building_slots = {
			building = infrastructure
			size < 8
		}
		is_controlled_by = ROOT
	}
}

###

infra_bypass_2_tr = {
	custom_trigger_tooltip = {
		tooltip = infra_bypass_2_tt
		free_building_slots = {
			building = infrastructure
			size < 5
		}
		is_controlled_by = ROOT
	}
}
###

infra_bypass_3_tr = {
	custom_trigger_tooltip = {
		tooltip = infra_bypass_3_tt
		free_building_slots = {
			building = infrastructure
			size < 1
		}
		is_controlled_by = ROOT
	}
}
###
hainan_trigger = {
	custom_trigger_tooltip = {
		tooltip = hainan_trigger_tt
		is_controlled_by = ROOT
		free_building_slots = {
			building = infrastructure
			size < 6
		}
	}
}
alu_tung_trigger = {
	custom_trigger_tooltip = {
		tooltip = alu_tung_trigger_tt
		is_controlled_by = ROOT
		free_building_slots = {
			building = infrastructure
			size < 3
		}
	}
}
seven_infra_trigger = {
	custom_trigger_tooltip = {
		tooltip = seven_infra_trigger_tt
		is_controlled_by = ROOT
		free_building_slots = {
			building = infrastructure
			size < 4
		}
	}
}
max_infra_trigger = {
	custom_trigger_tooltip = {
		tooltip = infra_bypass_3_tt
		is_controlled_by = ROOT
		free_building_slots = {
			building = infrastructure
			size < 1
		}
	}
}

ai_cont_foc_prc = { #this is for AI to do continue focus
	OR = {
		is_ai = no
		NOT = {
			AND = {
				has_country_flag = j_focus_completed #10 foci completed
				OR = {
					has_idea = PRC_no_skilled_personnel_1
					has_idea = PRC_no_skilled_personnel_2
					has_idea = PRC_no_skilled_personnel_3
					has_idea = PRC_no_skilled_personnel_4
				}
				has_completed_focus = PRC_Chinese_Academy_of_Sciences
			}
		}
	}
}

RCM_GER_will_support = {
	country_exists = GER
	NOT = {
		has_war_with = GER
	}
	if = {
		limit = {
			NOT = {
				SOV = {
					OR = {
						has_war_together_with = GER
						is_in_faction_with = GER
						has_non_aggression_pact_with = GER
					}
				}
			}
		}
		NOT = {
			is_in_faction_with = SOV
		}
	}
	if = {
		limit = {
			NOT = {
				ENG = {
					OR = {
						has_war_together_with = GER
						is_in_faction_with = GER
						has_non_aggression_pact_with = GER
					}
				}
			}
		}
		NOT = {
			is_in_faction_with = ENG
		}
	}
	if = {
		limit = {
			NOT = {
				USA = {
					OR = {
						has_war_together_with = GER
						is_in_faction_with = GER
						has_non_aggression_pact_with = GER
					}
				}
			}
		}
		NOT = {
			is_in_faction_with = USA
		}
	}

	if = {
		limit = {
			NOT = {
				OR = {
					has_government = neutrality
					GER = {
						has_government = ROOT
					}
				}
			}
		}
		GER_will_support = yes
	}
}
RCM_SOV_will_support = {
	country_exists = SOV
	NOT = {
		has_war_with = SOV
	}

	if = {
		limit = {
			NOT = {
				JAP = {
					OR = {
						has_war_together_with = SOV
						is_in_faction_with = SOV
						has_non_aggression_pact_with = SOV
					}
				}
			}
		}
		NOT = {
			is_in_faction_with = JAP
		}
	}
	if = {
		limit = {
			NOT = {
				GER = {
					OR = {
						has_war_together_with = SOV
						is_in_faction_with = SOV
						has_non_aggression_pact_with = SOV
					}
				}
			}
		}
		NOT = {
			is_in_faction_with = GER
		}
	}

	if = {
		limit = {
			NOT = {
				OR = {
					has_government = neutrality
					SOV = {
						has_government = ROOT
					}
				}
			}
		}
		SOV_will_support = yes
	}
}
RCM_USA_UK_will_support = {
	OR = {
		AND = {
			country_exists = ENG
			NOT = {
				has_war_with = ENG
			}
			if = {
				limit = {
					NOT = {
						GER = {
							OR = {
								has_war_together_with = ENG
								is_in_faction_with = ENG
								has_non_aggression_pact_with = ENG
							}
						}
					}
				}
				NOT = {
					is_in_faction_with = GER
				}
			}
			if = {
				limit = {
					NOT = {
						OR = {
							has_government = neutrality
							ENG = {
								has_government = ROOT
							}
						}
					}
				}
				ENG_will_support = yes
			}
		}
		AND = {
			country_exists = USA
			NOT = {
				has_war_with = USA
			}
			if = {
				limit = {
					NOT = {
						GER = {
							OR = {
								has_war_together_with = USA
								is_in_faction_with = USA
								has_non_aggression_pact_with = USA
							}
						}
					}
				}
				NOT = {
					is_in_faction_with = GER
				}
			}
			if = {
				limit = {
					NOT = {
						JAP = {
							OR = {
								has_war_together_with = USA
								is_in_faction_with = USA
								has_non_aggression_pact_with = USA
							}
						}
					}
				}
				NOT = {
					is_in_faction_with = JAP
				}
			}
			if = {
				limit = {
					NOT = {
						OR = {
							has_government = neutrality
							USA = {
								has_government = ROOT
							}
						}
					}
				}
				USA_will_support = yes
			}
		}
	}
}
RCM_UK_will_support = {
	country_exists = ENG
	NOT = {
		has_war_with = ENG
	}
	if = {
		limit = {
			NOT = {
				GER = {
					OR = {
						has_war_together_with = ENG
						is_in_faction_with = ENG
						has_non_aggression_pact_with = ENG
					}
				}
			}
		}
		NOT = {
			is_in_faction_with = GER
		}
	}
	if = {
		limit = {
			NOT = {
				OR = {
					has_government = neutrality
					ENG = {
						has_government = ROOT
					}
				}
			}
		}
		ENG_will_support = yes
	}
}
RCM_USA_will_support = {
	country_exists = USA
	NOT = {
		has_war_with = USA
	}
	if = {
		limit = {
			NOT = {
				GER = {
					OR = {
						has_war_together_with = USA
						is_in_faction_with = USA
						has_non_aggression_pact_with = USA
					}
				}
			}
		}
		NOT = {
			is_in_faction_with = GER
		}
	}
	if = {
		limit = {
			NOT = {
				JAP = {
					OR = {
						has_war_together_with = USA
						is_in_faction_with = USA
						has_non_aggression_pact_with = USA
					}
				}
			}
		}
		NOT = {
			is_in_faction_with = JAP
		}
	}
	if = {
		limit = {
			NOT = {
				OR = {
					has_government = neutrality
					USA = {
						has_government = ROOT
					}
				}
			}
		}
		USA_will_support = yes
	}
}


###CHI_inch_blood
CHI_inch_blood_up_trigger = {
	event_target:WTT_current_china_leader = {
		OR = {
			AND = {
				has_idea = CHI_inch_blood_1
				surrender_progress > 0.06
			}
			AND = {
				has_idea = CHI_inch_blood_2
				surrender_progress > 0.12
			}
			AND = {
				has_idea = CHI_inch_blood_3
				surrender_progress > 0.18
			}
			AND = {
				has_idea = CHI_inch_blood_4
				surrender_progress > 0.24
			}
			AND = {
				has_idea = CHI_inch_blood_5
				surrender_progress > 0.30
			}
			AND = {
				has_idea = CHI_inch_blood_6
				surrender_progress > 0.36
			}
			AND = {
				has_idea = CHI_inch_blood_7
				surrender_progress > 0.42
			}
			AND = {
				has_idea = CHI_inch_blood_8
				surrender_progress > 0.48
			}
			AND = {
				has_idea = CHI_inch_blood_9
				surrender_progress > 0.54
			}
		}
	}
}
unit_limiter_trigger = {
	AND = {
		#is_exempt_from_division_limit = no
		surrender_progress < 0.5
		OR = {
			AND = {
				tag = GER
				has_completed_focus = GER_danzig_or_war
				surrender_progress < 0.1
			}
			AND = {
				tag = ENG
				GER = {
					OR = {
						AND = {
							NOT = { has_war_with = ENG }
							date > 1943.1.1
						}
						surrender_progress > 0.5
					}
				}
			}
			AND = {
				tag = SOV
				date > 1943.1.1
				has_war = no
			}
			AND = {
				tag = USA
				date > 1944.1.1
				has_war = no
			}
			NOT = {
				OR = {
					tag = GER
					tag = SOV
					tag = ENG
					tag = USA
				}
			}
		}
		NOT = {
			OR = {
				AND = {
					JAP = {
						is_ai = yes
					}
					is_puppet_of = JAP
				}
				tag = JAP
				is_legit_china = yes
				tag = CHI
				tag = RAJ
				tag = AST
			}
		}
		check_variable = {
			num_divisions > num_of_military_factories
		}
	}
}

CHI_inch_blood_down_trigger = {
	event_target:WTT_current_china_leader = {
		OR = {
			AND = {
				has_idea = CHI_inch_blood_2
				surrender_progress < 0.06
			}
			AND = {
				has_idea = CHI_inch_blood_3
				surrender_progress < 0.12
			}
			AND = {
				has_idea = CHI_inch_blood_4
				surrender_progress < 0.18
			}
			AND = {
				has_idea = CHI_inch_blood_5
				surrender_progress < 0.24
			}
			AND = {
				has_idea = CHI_inch_blood_6
				surrender_progress < 0.30
			}
			AND = {
				has_idea = CHI_inch_blood_7
				surrender_progress < 0.36
			}
			AND = {
				has_idea = CHI_inch_blood_8
				surrender_progress < 0.42
			}
			AND = {
				has_idea = CHI_inch_blood_9
				surrender_progress < 0.48
			}
			AND = {
				has_idea = CHI_inch_blood_10
				surrender_progress < 0.54
			}
		}
	}
}

CKS_in_charge = {
	custom_trigger_tooltip = {
		tooltip = has_country_leader_CKS_tt
		OR = {
			has_country_leader = { id = 5001 ruling_only = yes name = "Chiang Kai-shek" }
			has_country_leader = { id = 5002 ruling_only = yes name = "Chiang Kai-shek" }
			has_country_leader = { id = 5003 ruling_only = yes name = "Chiang Kai-shek" }
		}
	}
}
FK_in_charge = {
	custom_trigger_tooltip = {
		tooltip = has_country_leader_FK_tt
		OR = {
			has_country_leader = { id = 6001 ruling_only = yes name = "Fumimaro Konoe" }
			has_country_leader = { id = 6002 ruling_only = yes name = "Fumimaro Konoe" }
		}
	}
}
FK_NOT_in_charge = {
	custom_trigger_tooltip = {
		tooltip = has_country_NOT_leader_FK_tt
		NOT = {
			OR = {
				has_country_leader = { id = 6001 ruling_only = yes name = "Fumimaro Konoe" }
				has_country_leader = { id = 6002 ruling_only = yes name = "Fumimaro Konoe" }
			}
		}
	}
}