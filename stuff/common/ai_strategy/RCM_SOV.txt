# befriend
# conquer
# antagonize
# build_ship
# build_army
# unit_ratio
# build_building
# research_tech
# garrison
# protect
## influence
#SOV_GER_is_NOT_weak = {
#
#	enable = {
#		tag = SOV
#		check_variable = {
#			This.infantry_equipment_deficit > 9000
#		}
#	}
#
#	abort = {
#		#always = no  #MODDED
#		check_variable = {
#			This.infantry_equipment_deficit < 4000
#		}
#	}
#
#	ai_strategy = {
#		type = consider_weak
#		id = "GER"
#		value = -120 #<MODDED
#	}
#	ai_strategy = {
#		type = consider_weak
#		id = "ROM"
#		value = -120 #<MODDED
#	}
#}
SOV_GER_is_weak = {

	enable = {
		SOV = {
			is_ai = yes
		}
		tag = GER
	}

	abort = {
		always = no  #MODDED
	}

	ai_strategy = {
		type = consider_weak
		id = "SOV"
		value = 250 #<MODDED
	}
}
SOV_GER_is_weak = {

	enable = {
		SOV = {
			is_ai = no
		}
		tag = GER
		casualties_inflicted_by = {
			opponent = SOV
			thousands < 500
		}
	}

	abort = {
		#always = no  #MODDED
		tag = GER
		casualties_inflicted_by = {
			opponent = SOV
			thousands > 500
		}
	}

	ai_strategy = {
		type = consider_weak
		id = "SOV"
		value = 200 #<MODDED
	}
}
SOV_GER_is_weak_2 = {

	enable = {
		tag = GER
		SOV = {
			is_ai = no
		}
		casualties_inflicted_by = {
			opponent = SOV
			thousands < 1000
		}
		casualties_inflicted_by = {
			opponent = SOV
			thousands > 500
		}
	}

	abort = {
		#always = no  #MODDED
		tag = GER
		casualties_inflicted_by = {
			opponent = SOV
			thousands > 1000
		}
	}

	ai_strategy = {
		type = consider_weak
		id = "SOV"
		value = 150 #<MODDED
	}
}
SOV_GER_is_weak_2 = {

	enable = {
		tag = GER
		SOV = {
			is_ai = no
		}
		casualties_inflicted_by = {
			opponent = SOV
			thousands < 3000
		}
		casualties_inflicted_by = {
			opponent = SOV
			thousands > 1000
		}
	}

	abort = {
		#always = no  #MODDED
		tag = GER
		casualties_inflicted_by = {
			opponent = SOV
			thousands > 3000
		}
	}

	ai_strategy = {
		type = consider_weak
		id = "SOV"
		value = 120 #<MODDED
	}
}
SOV_JAP_is_weak_2 = {

	enable = {
		tag = SOV
		has_war_with = JAP
		has_war_together_with = ENG
		NOT = {
			country_exists = GER
		}
	}

	abort_when_not_enabled = yes

	ai_strategy = {
		type = consider_weak
		id = "JAP"
		value = 120 #<MODDED
	}
}
SOV_SIK_VOL = {

	enable = {
		tag = SOV
		SIK = {
			has_war = yes
			has_government = communism
			NOT = {
				has_war_with = SOV
			}
			OR = {
				has_idea = SIK_Support_idea
				has_idea = SIK_Weakened_ussr_influence
			}
		}
	}

	abort_when_not_enabled = yes

	ai_strategy = {
		type = send_volunteers_desire
		id = "SIK"
		value = 2500
	}
}