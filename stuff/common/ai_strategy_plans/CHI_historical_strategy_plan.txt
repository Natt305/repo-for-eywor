CHI_nationalist_historical_plan = {
	name = "Nationalist China historical plan"
	desc = "Essentially historical behavior for Nationalist China"

	enable = {
		original_tag = CHI
		OR = {
			AND = {
				is_historical_focus_on = yes
				has_game_rule = {
					rule = CHI_ai_behavior
					option = DEFAULT
				}
			}
			has_country_flag = CHI_AI_RANDOM_HISTORICAL
			has_game_rule = {
				rule = CHI_ai_behavior
				option = HISTORICAL
			}
		}
	}
	abort = {
		JAP = {
			OR = {
				has_completed_focus = JAP_strengthen_civilian_government
				has_global_flag = kodoha_won
				is_in_faction_with = ENG
				is_in_faction_with = USA
			}
		}
		has_game_rule = {
			rule = CHI_ai_behavior
			option = ALTERNATE
		}
		MAN = {
			is_subject = no
		}
	}
	ai_national_focuses = {
		centralizing_power
		CHI_government_bailout_focus
		CHI_nationalize_industry_focus
		CHI_nationalize_industry_ii_focus
		CHI_nationalize_industry_iii_focus
		CHI_split_plm_focus
		CHI_monetary_reform			
		constit_draft_focus
		CHI_foreign_help_focus
		CHI_inst_reform_focus		
		CHI_Encirclement_Campaign_focus
		CHI_national_political_council_focus		
		CHI_National_Defense_Council_focus
		CHI_pressure_Ma_lin	
		one_party_tutelage		
		CHI_allies_help_focus		
		CHI_industry_focus
		CHI_mission_to_the_soviet_union
		CHI_infrastructure_effort_i		
		CHI_Establish_Hsikang_province
		CHI_purchase_tanks		
		CHI_axis_help_focus
		CHI_reorganize_nra
		strengthen_cc
		CHI_sov_pact_focus
		strengthen_the_kmt
		CHI_reinforce_new_life_movement
		strengthen_bs		
		CHI_fighter_purchases
		CHI_public_school_program_focus		
		CHI_reform_military_academy
		CHI_offer_xinjiang
		CHI_invite_the_flying_tigers
		CHI_allies_help_diplo_focus
		CHI_increase_equipment_production_focus
		CHI_whampoa_rifle
		CHI_logistical_reform_focus
		CHI_the_soviet_volunteer_group		
		CHI_unite_xibei
		found_cbis
		found_mbis
		CHI_Second_United_Front_focus		
		CHI_political_consultative_assembly_focus

		political_indoctrination
		espionage_network
		CHI_yi_dang_zhi_guo_focus

		CHI_streamlined_industry
		CHI_coast_infra_focus
		CHI_defend_river_focus

	}
	research = {
	}

	ideas = {

	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

}
