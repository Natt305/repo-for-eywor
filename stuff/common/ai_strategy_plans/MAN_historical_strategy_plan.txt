MAN_historical_plan = {
	name = "Manchukuo historical plan"
	desc = "Historical behavior for Manchukuo"

	enable = {
		original_tag = MAN
		is_historical_focus_on = yes
	}
	abort = {
		JAP = {
			has_completed_focus = JAP_strengthen_civilian_government
		}
	}

	ai_national_focuses = {
		MAN_obedience
		MAN_exploitation_focus
		MEN_State_religion
		MAN_Policy_of_Japanese_immigrants
		MAN_invite_japanese_settlers
		MAN_Labour_law
		MAN_Tenko_the_leftists_youth
		MAN_raid_the_social_research_unit
	}

	focus_factors = {
		
	}

	research = {

	}

	ideas = {

	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

}
