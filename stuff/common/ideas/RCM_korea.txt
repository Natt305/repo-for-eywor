ideas = {

	political_advisor = {

		cho_bongam = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { progressive_activist }
		}

		shin_ikhui = {

			picture = generic_political_advisor_asia_2
			visible = {
				NOT = {
					has_government = communism
				}
			}	
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			traits = { popular_figurehead }
		}

		cho_byeongok = {

			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { smooth_talking_charmer }
		}
		
		kim_byeongro = {
		
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { democratic_reformer }
		}
		
		ahn_jaehong = {

			picture = generic_political_advisor_asia_2
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { compassionate_gentleman }
		}
		
		jang_myeon = {

			picture = generic_political_advisor_asia_3
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { ideological_crusader }
		}
		
		yi_suntak = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { captain_of_industry }
		}
		
		yi_beom_seok = {
		
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { fascist_demagogue }
		}
		
		lee_siyeong = {
		
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { silent_workhorse }
		}
		
		jang_taeksang = {
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { backroom_backstabber }
		}
		
		park_heon_young = {
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { communist_revolutionary }
		}
		
		cho_dong_ho = {

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { socialist_idealist }
		}
		
		###NORTH KOREA MINISTERS###
		
		cho_mansik = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				has_government = communism
			}
			traits = { socialist_idealist }
		}
		
		prk_park_heonyeong = { #duplicate of park_heon_young
		
			picture = generic_political_advisor_asia_2

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { ideological_crusader }
		}
		
		choi_yonggeon = {
		
			picture = generic_political_advisor_asia_3

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { communist_revolutionary }
		}
		
		kim_dubong = {
		
			picture = generic_political_advisor_asia_1

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				OR = {
					has_government = communism
					has_government = n_socialism
				}
			}
			
			traits = { popular_figurehead }
		}
		
		kim_chaek = {

			picture = generic_political_advisor_asia_2

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { backroom_backstabber }
		}
		
		jeong_juntaek = {
		
			picture = generic_political_advisor_asia_1

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { captain_of_industry }
		}
		
		park_seongcheol = {

			picture = generic_political_advisor_asia_3

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { smooth_talking_charmer }
		}
		
		kim_il = {

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			picture = generic_political_advisor_asia_1

			
			visible = {
				has_government = communism
			}
			
			traits = { war_industrialist }
		}
		
		park_geumcheol = {

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			picture = generic_political_advisor_asia_2

			
			visible = {
				has_government = communism
			}
			
			traits = { silent_workhorse }
		}
		
		lee_yunyeong = {

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			picture = generic_political_advisor_asia_2

			
			visible = {
				OR = {
					has_government = neutrality
					has_government = democratic
				}
			}
			
			traits = { compassionate_gentleman }
		}
		
		kim_changbong = {

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			picture = generic_political_advisor_asia_3

			
			visible = {
				has_government = communism
			}
			
			traits = { quartermaster_general }
		}
	}

	theorist = {
		lee_hyeonggeun = {

			picture = generic_army_asia_2
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			research_bonus = {
				land_doctrine = 0.10
			}
			
			traits = { military_theorist }
		}
		
		choi_yongdeok = {

			picture = generic_air_asia_2
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			research_bonus = {
				air_doctrine = 0.10
			}
			
			traits = { air_warfare_theorist }
		}
		
		###NORTH KOREA###
		
		kim_ung = {

			picture = generic_army_asia_3
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			research_bonus = {
				land_doctrine = 0.10
			}
			
			traits = { military_theorist }
		}
	}	

	army_chief = {
		
		chae_byeongdeok = {
			
			picture = generic_army_asia_1

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { army_chief_old_guard }
			
			ai_will_do = {
				factor = 1
			}
		}
		

		lee_eungjun = {

			picture = generic_army_asia_3
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { army_chief_offensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		kim_hongil = {

			picture = generic_army_asia_2
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { army_chief_defensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		###NORTH KOREA###
		
		kim_gwang_hyeop = {

			picture = generic_army_asia_2
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			traits = { army_chief_drill_2 }
			
			visible = {
				has_government = communism
			}
			
			ai_will_do = {
				factor = 1
			}
		}
		
		kang_geon = {

			picture = generic_army_asia_1
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			traits = { army_chief_old_guard }
			
			visible = {
				has_government = communism
			}
			
			ai_will_do = {
				factor = 1
			}
		}
	}
	
	navy_chief = {
		
		soon_wonil = {
			
			picture = generic_navy_asia_1

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}		
			traits = { navy_chief_reform_2 }
			
			ai_will_do = {
				factor = 1
			}
		}


		park_okgyu = {
			
			picture = generic_navy_asia_3

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}		
			traits = { navy_chief_decisive_battle_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		###NORTH KOREA###
		
		han_ilmu = {
			
			picture = generic_navy_asia_2

			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
	
			traits = { navy_chief_commerce_raiding_2 }
			
			visible = {
				has_government = communism
			}
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	air_chief = {
		
		kim_jeongryeol = {
			
			picture = generic_air_asia_2
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { air_chief_ground_support_2 }
			
			ai_will_do = {
				factor = 1
			}
		}


		kim_sin = {

			picture = generic_air_asia_3
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { air_chief_reform_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		###NORTH KOREA###
		
		lee_hwal = {
			
			picture = generic_air_asia_2
				
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
					
			traits = { air_chief_night_operations_2 }
			
			visible = {
				has_government = communism
			}
			
			ai_will_do = {
				factor = 1
			}
		}
		
		wang_ryeon = {
			
			picture = generic_air_asia_1
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { air_chief_reform_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}
	
	high_command = {

		lee_jongchan = {

			picture = generic_army_asia_3
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { army_logistics_1 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		shin_hyeonjun = {

			picture = generic_army_asia_1
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { navy_amphibious_assault_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		jeong_ilgwon = {

			picture = generic_air_asia_3
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { army_regrouping_1 }
			
			ai_will_do = {
				factor = 1
			}
		}


		jang_deokchang = {

			picture = generic_navy_asia_1
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { air_air_superiority_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		kim_seokwon = {

			picture = generic_army_asia_2
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			visible = {
				NOT = {
					has_government = communism
				}
			}
			traits = { army_infantry_1 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		###NORTH KOREA###
		
		kim_mujeong = {

			picture = generic_army_asia_2
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { army_artillery_3 }
				
			ai_will_do = {
				factor = 1
			}
		}
		
		nam_il = {

			picture = generic_army_asia_1
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { army_logistics_1	}
				
			ai_will_do = {
				factor = 1
			}
		}
		
		choi_gwang = {

			picture = generic_army_asia_3
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { army_infantry_3 }
				
			ai_will_do = {
				factor = 1
			}
		}
		
		bang_hosan = {

			picture = generic_army_asia_2
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { army_armored_1 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		ryu_gyeongsu = {

			picture = generic_army_asia_1
			
			allowed = {
				OR = {
					original_tag = KOR
					original_tag = DPR
				}
			}
			
			visible = {
				has_government = communism
			}
			
			traits = { army_commando_2	}
						
			ai_will_do = {
				factor = 1
			}
		}
	}
}