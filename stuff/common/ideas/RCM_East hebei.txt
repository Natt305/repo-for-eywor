ideas = {
	political_advisor = {
		EHB_harold_ickes = {
			
			
			allowed = {
				original_tag = "EHB"
			}
			
			available = {
				is_puppet = no
			}
			
			traits = { democratic_reformer }
	
			on_add = {
				#country_event = political.13
			}
	
			do_effect = {
				NOT = {
					has_government = democratic
				}
			}
			picture = generic_democratic_asia
			ai_will_do = {
				factor = 0
			}
		}
		
		EHB_charles_coughlin = {
			
			
			allowed = {
				original_tag = "EHB"
			}
			

			
			traits = { fascist_demagogue }
	
			on_add = {
				#country_event = political.7
			}
	
			do_effect = {
				NOT = {
					has_government = fascism
				}
			}
			picture = generic_fascist_asia
			ai_will_do = {
				factor = 0
			}
		}
		EHB_pierre_dupong = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { silent_workhorse }
		}

		EHB_psre_dupong = {

			picture = generic_political_advisor_asia_1
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { compassionate_gentleman }
		}

		EHB_pierre_krier = {

			picture = generic_political_advisor_asia_3
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { captain_of_industry }
		}
	}
	# MIEHBARY
	army_chief = {
		
		EHB_emile_speller = {
			
			picture = generic_army_asia_1
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { army_chief_defensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		EHB_alexander_von_falkenhausen = {
			
			picture = generic_army_asia_2
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { army_chief_offensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}	
	}

	air_chief = {
		
		EHB_guillaume_soisson = {
			
			picture = generic_air_asia_3
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { air_air_combat_training_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		EHB_nicholas_diedrich = {
			
			picture = generic_air_asia_1
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { air_chief_ground_support_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
	}

	navy_chief = {
		
		EHB_paul_medinger = {
			
			picture = generic_navy_asia_3
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { navy_chief_decisive_battle_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		EHB_henri_ahnen = {
			
			picture = generic_navy_asia_1
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { navy_chief_maneuver_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	high_command = {

		EHB_francois_schammel = {

			picture = generic_army_asia_1
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { army_infantry_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		EHB_aloyse_glodt = {

			picture = generic_army_asia_3
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		EHB_damien_roeser = {

			picture = generic_air_asia_2
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { air_air_superiority_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		EHB_pierre_fischbach = {

			picture = generic_navy_asia_1
			
			allowed = {
				original_tag = EHB
			}
			
			traits = { navy_fleet_logistics_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	theorist = {

		EHB_alex_federspiel = {
					
			picture = generic_army_asia_1

			allowed = {
				original_tag = EHB
			}
			
			research_bonus = {
				land_doctrine = 0.10
			}
			
			traits = { military_theorist }
		}

		EHB_nicolas_de_dixmude = {
					
			picture = generic_air_asia_1
				
			allowed = {
				original_tag = EHB
			}
			
			research_bonus = {
				air_doctrine = 0.10
			}
			
			traits = { air_warfare_theorist }
		}

		EHB_florent_destriveaux = {
					
			picture = generic_navy_asia_1
				
			allowed = {
				original_tag = EHB
			}
			
			research_bonus = {
				naval_doctrine = 0.10
			}
			
			traits = { naval_theorist }
		}
	}

}