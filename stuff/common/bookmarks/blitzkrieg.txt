bookmarks = {
	bookmark = {
		name = "BLITZKRIEG_NAME"
		desc = "BLITZKRIEG_DESC"
		date = 1939.8.14.12
		picture = "GFX_select_date_1939"
		default_country = "GER"

		FRA = {
			history = FRA_BLITZKRIEG_DESC
			ideology = democratic
			ideas={
				FRA_victors_of_wwi
				FRA_disjointed_government
				FRA_protected_by_the_maginot_line
			}
			focuses = {
				FRA_initiate_government_reform
				FRA_support_status_quo
				FRA_revise_versailles
			}
		}
		USA = {
			history = USA_BLITZKRIEG_DESC
			ideology = democratic
			ideas = {
				home_of_the_free
				great_depression_3
				air_war_plans_division_focus
				USA_war_department
				new_deal
			}
			focuses = {
				USA_war_plan_black
				USA_arsenal_of_democracy
				USA_selective_training_act
			}
		}
		ENG = {
			history = ENG_BLITZKRIEG_DESC
			ideology = democratic
			ideas = {
				stiff_upper_lip
				ENG_the_war_to_end_all_wars
				ENG_george_vi
				british_austerity_idea
				ENG_colonial_elite
			}
			focuses = {
				ENG_war_with_japan
				bomber_command_focus
				maud_focus
			}

		}
		GER = {
			history = GER_BLITZKRIEG_DESC
			ideology = fascism
			ideas = {
				sour_loser
				general_staff
				GER_mefo_bills_15
				GER_autarky_idea
			}
			focuses = {
				GER_weserubung
				GER_plan_z
				GER_ussr_war_goal
			}
		}
		ITA = {
			history = ITA_BLITZKRIEG_DESC
			ideology = fascism
			ideas={
				vittoria_mutilata
				victor_emmanuel
			}
			focuses = {
				ITA_pact_of_steel
				ITA_spanish_italian_faction
				ITA_italy_first
			}
		}
		JAP = {
			history = JAP_BLITZKRIEG_DESC
			ideology = fascism
			ideas={
				state_shintoism
				emperor_showa #MODDED
				JAP_zaibatsus #MODDED
			}
			focuses = {
				southern_expansion_focus #MODDED
				divine_wind_focus #MODDED
				showa_restoration_focus #MODDED
			}
		}

		"CHI"={
			history = "CHI_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				chiang
				incompetent_heavy_industry
				the_blueshirts
			}
			focuses = {
				CHI_strengthen_chiangs_rule
				CHI_reorganize_nra
				three_principle_of_the_people
			}
		}

		SOV = {
			history = SOV_BLITZKRIEG_DESC
			ideology = communism
			ideas = {
				trotskyite_plot_purged
				home_of_revolution
				officers_purged
				nkvd_2
			}
			focuses = {
				SOV_move_industry_to_urals
				SOV_claims_on_baltic
				SOV_war_with_uk
			}
		}

		"---"={
			history = "OTHER_BLITZKRIEG_DESC"
		}

		# minors from DLC ####
		"POL"={
			minor = yes
			history = "POL_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {

				deterrence
			}
			focuses = {
				POL_the_baltic_alliance
				POL_draw_closer_to_britain
				POL_seek_accommodation_with_germany
			}
		}
		"CAN"={
			minor = yes
			history = "CAN_BLITZKRIEG_DESC"
			ideology = democratic
			ideas = {
				CAN_great_depression_1
				CAN_conscription_crisis
			}
			focuses = {
				CAN_skewer_the_eagle
				CAN_north_american_alliance
				CAN_join_comintern
			}
		}
		"AST"={
			minor = yes
			history = "AST_BLITZKRIEG_DESC"
			ideology = democratic
			ideas = {
				AST_great_depression_1
			}
			focuses = {
				AST_never_another_gallipoli
				AST_support_indonesian_uprising
				AST_delegation_to_china
			}
		}
		"NZL"={
			minor = yes
			history = "NZL_BLITZKRIEG_DESC"
			ideology = democratic
			ideas = {

			}
			focuses = {
				NZL_bob_semple_tank
				NZL_independent_new_zealand
				NZL_maori_volunteers
			}
		}
		"SAF"={
			minor = yes
			history = "SAF_BLITZKRIEG_DESC"
			ideology = democratic
			ideas = {
				SAF_ossewabrandwag
				SAF_history_of_segregation
			}
			focuses = {
				SAF_secure_interests_in_africa
				SAF_support_the_german_coup
				SAF_anti_colonialist_crusade
			}
		}
		"RAJ"={
			minor = yes
			history = "RAJ_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				RAJ_agrarian_society
				RAJ_princely_states
			}
			focuses = {
				RAJ_all_india_forward_bloc
				RAJ_indian_gurkhas
				RAJ_the_smiling_buddha
			}
		}

		"HUN"={
			minor = yes
			history = "HUN_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				HUN_treaty_of_triannon_3
				HUN_interventionism
				HUN_strengthen_fascists
			}
			focuses = {
				HUN_elect_a_king
				HUN_secret_rearmament
				HUN_renounce_the_treaty_of_trianon
			}
		}

		"ROM"={
			minor = yes
			history = "ROM_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				ROM_king_carol_ii_hedonist
				ROM_preserve_greater_romania
				ROM_iron_guard
			}
			focuses = {
				ROM_balkans_dominance
				ROM_institute_royal_dictatorship
				ROM_preserve_greater_romania
			}
		}

		"YUG"={
			minor = yes
			history = "YUG_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				anti_german_military
				YUG_idea_croatian_opposition
				YUG_idea_orthodox_church_support
			}
			focuses = {
				YUG_join_axis
				YUG_join_allies
				YUG_the_ik_3
			}
		}

		"MEX"={
			minor = yes
			history = "MEXICO_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				MEX_church_power_1
				MEX_politicised_army
				MEX_ejido_worker_militias
				MEX_capital_reserves
			}
			focuses = {
				MEX_focus_bolivarian_alliance
				MEX_focus_aztec_eagles
				MEX_focus_the_gold_shirts
			}
		}

		"HOL"={
			minor = yes
			history = "NETHERLANDS_BLITZKRIEG_DESC"
			ideology = democratic
			ideas = {
				HOL_wilhelmina
				HOL_shell_shocked_spectator_5
				HOL_de_crisisjaren_1
			}
			focuses = {
				HOL_continue_the_war_in_batavia
				HOL_liberation
				HOL_prepare_the_inundation_lines
			}
		}

		"FIC"={
			minor = yes
			history = "FIC_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				FIC_colonial_loan_idea
				FIC_iliterate_population
				fic_indochinese_manpower
			}
			focuses = {
				FIC_indochina_fights_on
				FIC_defend_against_siam
				FIC_at_the_helm_of_indochina
			}
		}

		"MON"={
			minor = yes
			history = "MON_BLITZKRIEG_DESC"
			ideology = communism
			ideas = {
				MON_nomadic
				MON_power_struggle
			}
			focuses = {
				MON_militant_buddhism
				MON_russian_colony
				KMT_ties
			}
		}
		"ECA"={
			minor = yes
			history = "EHB_GATHERING_STORM_DESC"
			ideology = fascism
			ideas = {
				MON_nomadic
				MEN_reformed_currency
				MEN_standing_advisors
			}
			focuses = {
				MEN_bank
				MEN_suiyuan_offensive
				MEN_independence_war
			}
		}
		"EHB"={
			minor = yes
			history = "EHB_GATHERING_STORM_DESC"
			ideology = fascism
			ideas = {
				JPG_illegal_regime_1
				PRC_government_corruption
				CHI_fractured_administration_idea_1
			}
			focuses = {
				JPG_seek_japanese_support
				JPG_army_expansion
				JPG_reorganization_of_the_regime
			}
		}
		"RFM"={
			minor = yes
			history = "EHB_GATHERING_STORM_DESC"
			ideology = fascism
			ideas = {
				JPG_illegal_regime_1
				PRC_government_corruption
				CHI_fractured_administration_idea_1
			}
			focuses = {
				JPG_seek_japanese_support
				JPG_army_expansion
				JPG_reorganization_of_the_regime
			}
		}
		"PRC"={
			minor = yes
			history = "PRC_BLITZKRIEG_DESC"
			ideology = communism
			ideas = {
				Long_March_3
				PRC_no_skilled_personnel_1
				incompetent_heavy_industry
			}
			focuses = {
				PRC_Trotskyism_faction
				PRC_A_New_Countey
				PRC_PLA
			}
		}
		"MAN"={
			minor = yes
			history = "MAN_BLITZKRIEG_DESC"
			ideology = fascism
			ideas = {
				MAN_kwantung_veto
				MAN_five_year_plan_industry
				MAN_low_legitimacy_4
			}
			focuses = {
				MAN_obedience
				MAN_independence_war
				MAN_claim_the_mandate_of_heaven
			}
		}
		"SIK"={
			minor = yes
			history = "SIK_BLITZKRIEG_DESC"
			ideology = communism
			ideas = {
				SIK_Autocracy
				SIK_Support_idea
				SIK_Conflict_with_the_local_people
			}
			focuses = {
				SIK_Sinkiang_King
				SIK_The_western_revolution
				SIK_Turkic_land
			}
		}
		"SHX"={
			minor = yes
			history = "WARLORDS_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				CHI_long_term_economic_planning
				CHI_public_works
				CHI_embrace_the_opium_trade
			}
			focuses = {
				CHI_join_the_republican_government
				CHI_power_struggle
				CHI_leaves_the_KMT
			}
		}
		"YUN"={
			minor = yes
			history = "WARLORDS_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				CHI_long_term_economic_planning
				CHI_public_works
				CHI_embrace_the_opium_trade
			}
			focuses = {
				CHI_join_the_republican_government
				CHI_power_struggle
				CHI_leaves_the_KMT
			}
		}
		"GXC"={ 
			minor = yes
			history = "WARLORDS_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				CHI_long_term_economic_planning
				CHI_public_works
				CHI_embrace_the_opium_trade
			}
			focuses = {
				CHI_join_the_republican_government
				CHI_power_struggle
				CHI_leaves_the_KMT
			}
		}

		"XSM"={
			minor = yes
			history = "WARLORDS_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				CHI_long_term_economic_planning
				CHI_public_works
				CHI_embrace_the_opium_trade
			}
			focuses = {
				CHI_join_the_republican_government
				CHI_power_struggle
				CHI_leaves_the_KMT
			}
		}
		"NXM"={
			minor = yes
			history = "WARLORDS_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				CHI_long_term_economic_planning
				CHI_public_works
				CHI_embrace_the_opium_trade
			}
			focuses = {
				CHI_join_the_republican_government
				CHI_power_struggle
				CHI_leaves_the_KMT
			}
		}
		"XIK"={
			minor = yes
			history = "WARLORDS_BLITZKRIEG_DESC"
			ideology = neutrality
			ideas = {
				CHI_long_term_economic_planning
				CHI_public_works
				CHI_embrace_the_opium_trade
			}
			focuses = {
				CHI_join_the_republican_government
				CHI_power_struggle
				CHI_leaves_the_KMT
			}
		}



		effect = {
			randomize_weather = 12345 # <- Obligatory in every bookmark !
			#123 = { rain_light = yes }
		}
	}
}
