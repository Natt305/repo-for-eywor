RCM_invite_fujian_incident_leaders_back = {
	RCM_invite_hunag_qixiang = {
		icon = eng_trade_unions_support
		fire_only_once = yes
		cost = 50
		visible = { #can see
			has_defensive_war_with = JAP
			NOT = {
				country_exists = GXC
			}
			NOT = {
				custom_trigger_tooltip = {
					tooltip = joined_chi_GXC
					has_global_flag = joined_chi_GXC
				}
			}
		}
		available = { #can do
			has_defensive_war_with = JAP
			NOT = {
				custom_trigger_tooltip = {
					tooltip = joined_chi_GXC
					has_global_flag = joined_chi_GXC
				}
			}
		}
		complete_effect = { #run effect on selection
			set_global_flag = joined_chi_GXC
			create_corps_commander = {
				name = "Huang Qixiang" 
				portrait_path = "portrait_CHI_huang_qixiang.dds"
				traits = {}
				skill = 3
				attack_skill = 3
				defense_skill = 3
				planning_skill = 3
				logistics_skill = 2
			}
		}
		cancel_trigger = { #will abort

		}
		modifier = {
		}
		ai_will_do = {
			factor = 100
		}
	}
	RCM_invite_jiang_guangnai = {
		icon = eng_trade_unions_support
		fire_only_once = yes
		cost = 50
		visible = { #can see
			has_defensive_war_with = JAP
			NOT = {
				country_exists = GUD
			}
			NOT = {
				custom_trigger_tooltip = {
					tooltip = joined_chi_GUD
					has_global_flag = joined_chi_GUD
				}
			}
		}
		available = { #can do
			has_defensive_war_with = JAP
			NOT = {
				custom_trigger_tooltip = {
					tooltip = joined_chi_GUD
					has_global_flag = joined_chi_GUD
				}
			}
		}
		complete_effect = { #run effect on selection
			set_global_flag = joined_chi_GUD
			create_corps_commander = {
				name = "Jiang Guangnai"
				portrait_path = "portrait_CHI_Jiang_Guangnai.dds"
				traits = { war_hero }
				skill = 3
				attack_skill = 2
				defense_skill = 3
				planning_skill = 3
				logistics_skill = 2
			}
		}
		cancel_trigger = { #will abort

		}
		modifier = {
		}
		ai_will_do = {
			factor = 100
		}
	}
	RCM_invite_cai_tingkai = {
		icon = eng_trade_unions_support
		fire_only_once = yes
		cost = 50
		visible = { #can see
			has_defensive_war_with = JAP
			NOT = {
				country_exists = GUD
			}
			NOT = {
				custom_trigger_tooltip = {
					tooltip = joined_chi_GUD
					has_global_flag = joined_chi_GUD
				}
			}
		}
		available = { #can do
			has_defensive_war_with = JAP
			NOT = {
				custom_trigger_tooltip = {
					tooltip = joined_chi_GUD
					has_global_flag = joined_chi_GUD
				}
			}
		}
		complete_effect = { #run effect on selection
			set_global_flag = joined_chi_GUD
			create_corps_commander = {
				name = "Cai Tingkai"
				portrait_path = "portrait_CHI_Cai_Tingkai.dds"
				traits = { war_hero }
				skill = 3
				attack_skill = 3
				defense_skill = 3
				planning_skill = 2
				logistics_skill = 2
			}
		}
		cancel_trigger = { #will abort

		}
		modifier = {
		}
		ai_will_do = {
			factor = 100
		}
	}
}