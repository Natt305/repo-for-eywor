﻿# Division template historical names system. Is a new method of naming the divisions based on the names-group assigned to it's template.
# If we run out of all historical names, the names will be assigned in the old way.
#
# Each group has a made up tag. Use it if you want to append more, or replace existing names with the extra txt files (modding).
#
# for_countries - lists all countries that can use it. If empty, or entire tag is missing, all countries in the world can use it.
#
# can_use - is a trigger that locks/unlocks the group under specific circumstances. The trigger is in a country scope.
#
# division_types - is a list of tokens to corresponding unit types. A player can in fact use any group of names for a div.template
#                  however this tag is a helper for an automated choice (for AI, or if the group must switch on it's own, because
#                  for example the current one is no longer available due to the can_use trigger saying so).
#                  In automated choice, the division template must have at least 1 of the following types for it to be chosen.
#
# fallback_name - Is going to be used if we run out of the scripted historical names. If you want to use the old division naming
#                 mechanics to be used for fallbacks, then just skip this option.
#
# unordered - It's a list of historical division names that did not have a number. Regardless if such names happened in history or not
#             this option is available here mainly for a mods.
#
# ordered - Is a list of all historical names. 
#           Numbers must start from 1 and up. 
#           Numbers can't repeat in one scope.
#           If you script the name for this group, at the same number (for example in a mod in another file), the name will be override.
#           All arguments between the brackets must be separated by spaces. Each argument is wrapped in "quotas".
#           1st argument = The name. It must contain either: 
#                          %d (for decimal number placement)
#                          %s (for string number placement - ROMAN numbers like XIV).
#           2nd argument is optional = A localizable text describing this historical division. The text will be visible in the tooltip
#                                      where you choose the historical division name.
#           3rd argument is optional = An URL preferably pointing to the WIKI. It's a future feature that is not currently working in
#                                      the current game version.
PRC_INF_01 = 
{
	name = "歩兵 Lü"

	for_countries = { PRC }

	can_use = { always = yes }

	division_types = { "infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { PRC_INF_01 }

	fallback_name = "八路軍 Hsinpian Ti %d Lü"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		343 = { "八路軍115師 Ti 343 Lü" }
		344 = { "八路軍115師 Ti 344 Lü" }
		358 = { "八路軍120師 Ti 358 Lü" }
		359 = { "八路軍120師 Ti 359 Lü" }
		385 = { "八路軍129師 Ti 385 Lü" }
		386 = { "八路軍129師 Ti 386 Lü" }
		  1 = { "八路軍 Hsinpian Ti %d Lü" }
		  2 = { "八路軍 Hsinpian Ti %d Lü" }
		  3 = { "八路軍 Hsinpian Ti %d Lü" }
		  4 = { "八路軍 Hsinpian Ti %d Lü" }
		  5 = { "八路軍 Hsinpian Ti %d Lü" }
		  6 = { "八路軍 Hsinpian Ti %d Lü" }
		  7 = { "八路軍 Hsinpian Ti %d Lü" }
		  8 = { "八路軍 Hsinpian Ti %d Lü" }
		  9 = { "八路軍 Hsinpian Ti %d Lü" }
		 10 = { "八路軍 Hsinpian Ti %d Lü" }
		 11 = { "八路軍 Ti 一縱隊 Ti 1 Lü" }
		 12 = { "八路軍 Ti 一縱隊 Ti 2 Lü" }
		 13 = { "八路軍 Ti 一縱隊 Ti 3 Lü" }
		 14 = { "八路軍 Ti 一縱隊 Ti 4 Lü" }
		 15 = { "八路軍 Ti 一縱隊 Ti 5 Lü" }
		 16 = { "八路軍 Ti 一縱隊 Ti 6 Lü" }
		 17 = { "八路軍 Ti 一縱隊 Ti 7 Lü" }
		 18 = { "八路軍 Ti 一縱隊 Ti 8 Lü" }
		 19 = { "八路軍 Ti 一縱隊 Ti 9 Lü" }
		 20 = { "八路軍 Ti 一縱隊 Ti 10 Lü" }
		 21 = { "八路軍 Ti 二縱隊 Ti 1 Lü" }
		 22 = { "八路軍 Ti 二縱隊 Ti 2 Lü" }
		 23 = { "八路軍 Ti 二縱隊 Ti 3 Lü" }
		 24 = { "八路軍 Ti 二縱隊 Ti 4 Lü" }
		 25 = { "八路軍 Ti 二縱隊 Ti 5 Lü" }
		 26 = { "八路軍 Ti 二縱隊 Ti 6 Lü" }
		 27 = { "八路軍 Ti 二縱隊 Ti 7 Lü" }
		 28 = { "八路軍 Ti 二縱隊 Ti 8 Lü" }
		 29 = { "八路軍 Ti 二縱隊 Ti 9 Lü" }
		 30 = { "八路軍 Ti 二縱隊 Ti 10 Lü" }
		 31 = { "八路軍 Ti 三縱隊 Ti 1 Lü" }
		 32 = { "八路軍 Ti 三縱隊 Ti 2 Lü" }
		 33 = { "八路軍 Ti 三縱隊 Ti 3 Lü" }
		 34 = { "八路軍 Ti 三縱隊 Ti 4 Lü" }
		 35 = { "八路軍 Ti 三縱隊 Ti 5 Lü" }
		 36 = { "八路軍 Ti 三縱隊 Ti 6 Lü" }
		 37 = { "八路軍 Ti 三縱隊 Ti 7 Lü" }
		 38 = { "八路軍 Ti 三縱隊 Ti 8 Lü" }
		 39 = { "八路軍 Ti 三縱隊 Ti 9 Lü" }
		 40 = { "八路軍 Ti 三縱隊 Ti 10 Lü" }
		 41 = { "八路軍 Ti 四縱隊 Ti 1 Lü" }
		 42 = { "八路軍 Ti 四縱隊 Ti 2 Lü" }
		 43 = { "八路軍 Ti 四縱隊 Ti 3 Lü" }
		 44 = { "八路軍 Ti 四縱隊 Ti 4 Lü" }
		 45 = { "八路軍 Ti 四縱隊 Ti 5 Lü" }
		 46 = { "八路軍 Ti 四縱隊 Ti 6 Lü" }
		 47 = { "八路軍 Ti 四縱隊 Ti 7 Lü" }
		 48 = { "八路軍 Ti 四縱隊 Ti 8 Lü" }
		 49 = { "八路軍 Ti 四縱隊 Ti 9 Lü" }
		 50 = { "八路軍 Ti 四縱隊 Ti 10 Lü" }
		 51 = { "八路軍 Ti 五縱隊 Ti 1 Lü" }
		 52 = { "八路軍 Ti 五縱隊 Ti 2 Lü" }
		 53 = { "八路軍 Ti 五縱隊 Ti 3 Lü" }
		 54 = { "八路軍 Ti 五縱隊 Ti 4 Lü" }
		 55 = { "八路軍 Ti 五縱隊 Ti 5 Lü" }
		 56 = { "八路軍 Ti 五縱隊 Ti 6 Lü" }
		 57 = { "八路軍 Ti 五縱隊 Ti 7 Lü" }
		 58 = { "八路軍 Ti 五縱隊 Ti 8 Lü" }
		 59 = { "八路軍 Ti 五縱隊 Ti 9 Lü" }
		 60 = { "八路軍 Ti 五縱隊 Ti 10 Lü" }
	}
}

PRC_INF_02 = 
{
	name = "游擊隊"

	for_countries = { PRC }

	can_use = { always = yes }

	division_types = { "infantry" }

	# Number reservation system will tie to another group.
	#link_numbering_with = { PRC_INF_01 }

	fallback_name = "新四軍 Ti %d支隊"

	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered =
	{
		  1 = { "新四軍 Ti 1支隊" }
		  2 = { "新四軍 Ti 2支隊" }
		  3 = { "新四軍 Ti 3支隊" }
		  4 = { "新四軍 Ti 4支隊" }

	}
}

